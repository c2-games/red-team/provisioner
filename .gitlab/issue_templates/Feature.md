### Summary

<!-- Describe the feature, why it's beneficial, and what work needs to be performed. -->
<!-- Keep features scoped as small as possible, while making sense logically -->


<!-- Also define requirements here as depends #XXX -->
depends #XXX

### Implementation Details

<!-- Describe potential implementation details/caveats, including example code blocks -->

```py
class Plugin:
    pass
```

### Validation Tests

<!-- How do we test that the feature is complete? It is important to be thorough here -->

- [ ] It works!


/label ~Feature
/milestone %Backlog