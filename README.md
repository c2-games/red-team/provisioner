# Provisioner

-------------
Automates the configuration and deployment of VMs with selected vulnerabilities
and misconfigured services for cyber security competitions. Provides a custom
Shell to select and configure ansible roles under the [plugins_path][1]

## Installation and requirements

-------------

- Install dependencies

```sh
apt install python3 python3-venv mongodb
```

*NOTE* A python version of 3.7 or 3.8 is recommended

- Install Ansible and PyMongo systemwide

```sh
sudo pip3 install ansible pymongo~=3.0
```

- Install c2games ansible galaxy

```sh
ansible-galaxy collection install c2games.opfor
```

- Clone Repository

```sh
git clone git@gitlab.com:c2-games/red-team/provisioner.git
cd provisioner
```

- Initialize Virtual Environment

```sh
python3 -m venv venv
source venv/bin/activate
```

- Upgrade pip

```sh
pip install -U pip
```

- Install project dependencies

```sh
pip install -r requirements.txt -r requirements-dev.txt
```

### Running in Docker

Override the following variables if required:

- `C2GAMES_PROVISIONER_PLUGIN_PATH`: defaults to
  `$PWD/../provisioner-plugins`

- `C2GAMES_ANSIBLE_KEY`: defaults to `~/.ssh/ansible_key`

- `C2GAMES_PROVISIONER_CONFIG`: defaults to `$PWD/provisioner/conf.yml`

#### Build & Start environment

```sh
docker-compose up --build -d
```

- Enter provisioner CLI

```sh
docker-compose exec provisioner provisioner
```

## Getting Started Guide

-------------

The provisioner currently has a fully tab-completable command line interface.
A Web UI is planned in future work.
The command line interface can be started by running the following:

```bash
python provisioner/cli.py
```

If you find the `cli.py` script not giving you a prompt, that is most likely
caused by an inability to communicate to a local mongodb instance.
Install it, or run mongo via docker and try again.

When `cli.py` is run, a scan of the `plugins_path` is performed.
The `plugins_path` can be configured in `provisioner/conf.yml`.
Example output would look like the following:

```sh
$ python provisioner/cli.py
loaded 0 plugin(s)
loaded 1 plugin(s)
loaded 31 plugin(s)
loaded 0 plugin(s)
[17:26:08] [default_event] provisioner )>
```

### Deploy a plugin

- Configure the host(s) to deploy to

`set job!targets 192.1.1.1`

- Select the plugins you wish to run against a host(s).
  Plugins and commands are tab completable, so typing `use` and then tabbing,
  will list the valid plugins available.
  To use a plugin, run the `use` command.

```sh
> use add_user
plugin now in use at: plugins.add_user:0
```

The name, difficulty, and description of loaded plugins can be viewed by
running `show plugins`.

You can choose to use several of the same plugins as well.
Each will get their own configuration ID associated with them.

```sh
[19:13:26] [default_event] provisioner )> use meta/add_admin_user
plugin now in use at: plugins.meta/add_admin_user:1
[19:13:26] [default_event] provisioner )> use meta/add_admin_user
plugin now in use at: plugins.meta/add_admin_user:2
```

You can also choose to give a unique name to the configuration ID instead of an
increasing numerical sequence:

```sh
[18:42:42] [default_event] provisioner )> use meta/add_admin_user:sarah
plugin now in use at: plugins.meta/add_admin_user:sarah
```

- View currently loaded plugins and order of execution

You can use the `show` command to view the current job specification.
You'll see the plugins configured to be run and the order they will execute in.
By default, they will run alphabetically since the altitude of the plugins all
default to 10000, when not configured by the user.
Plugins are executed in order from smaller to greater altitude.

```sh
)> set plugins.meta/add_admin_user:sarah!altitude 500
)> show job
...
Loaded Plugins                    Altitude
==========================================
plugins.meta/add_admin_user:sarah 500  
plugins.meta/add_admin_user:0     10000
plugins.meta/add_admin_user:1     10000
```

- Configure any job or plugin options

After you have loaded an option, you can configure any options it defines.
Below, we will change the username separately for each configuration ID.
Note: you can tab complete options as well.

```sh
> set plugins.add_user:0!
plugins.add_user:0!group     plugins.add_user:0!limit     plugins.add_user:0!tag
plugins.add_user:0!userlist  plugins.add_user:0!username

> set plugins.add_user:0!username test
> set plugins.add_user:1!username admin
> set plugins.add_user:2!username user
```

- Execute the plugins

Running the `deploy` command will first run some checks that everything is
configured that needs to be, and then will execute the specified plugins
against the target list.
The running ansible log will be displayed in the output at the configured
verbosity level.
For debugging, try setting `ansible!verbosity` to 2 or more.

### Optional, but helpful test setup

A test environment can be setup using vagrant.
For more information see [getting-started-with-vagrant][2]

## Dev testing

-------------

### Mongo DB

For developers, testing changes to the mongo DB driver can be done by utilizing
the upstream mongo docker image instead of installing mongodb.
To run mongo in docker:

``` bash
docker run -p 27017:27017 -d mongo
```

This runs MongoDB and forwards the appropriate port to the local system.
Set the MongoDB Address to `localhost` in the configuration file.

NOTE that this is necessary, and will not work, if MongoDB not is installed and
running on the local system.

[1]: https://gitlab.com/c2-games/red-team/provisioner/-/blame/master/provisioner/conf.yml?ref_type=heads#L7
[2]: https://www.ncaecybergames.org/getting-started-with-vagrant/
