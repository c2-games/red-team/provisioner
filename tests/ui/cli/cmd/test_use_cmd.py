import unittest
from unittest.mock import patch, MagicMock, call

from provisioner.structs.plugins import Plugin
from provisioner.ui.cli.shell import ProvisionerShell
from provisioner.structs.options import IntOption


class TestShell(unittest.TestCase):
    @patch('provisioner.ui.cli.shell.MongoDB')
    @patch('sys.exit')
    @patch('provisioner.ui.cli.shell.ProvisionerShell.cmdloop')
    def setUp(self, mock_shell: MagicMock, mock_exit: MagicMock, mock_mongo: MagicMock):
        mock_shell.return_value = 0
        self.shell = ProvisionerShell()
        altitude = IntOption(name='altitude', val=10000, oid='altitude')  # mock YML loader adding altitude
        self.test_plugin = Plugin(name='Test Plugin', id='testplugin', usage={}, options=[altitude])
        self.test_plugin2 = Plugin(name='Test Plugin 2', id='testplugin2', usage={}, options=[altitude])

        config = self.shell.ctx.job.options
        self.subconf = config.resolve_config('plugins')  # parent is plugins by default for normal plugins

        self.shell.ctx.plugin_store.add_plugin('testplugin', self.test_plugin, [])
        self.shell.ctx.plugin_store.add_plugin('testplugin2', self.test_plugin2, [])

        # Set up some configurations
        self.subconf.add_plugin(self.test_plugin, 'config_0')
        self.subconf.add_plugin(self.test_plugin, 'config_1')
        self.subconf.add_plugin(self.test_plugin2, 'config_0')
        self.subconf.add_plugin(self.test_plugin2, 'config_1')

    def test_parse_args(self):
        # calling with bad argument (CommandShortCircuit)
        self.assertEqual(self.shell.use_command.run(self.shell, ['--asdf']), 1)
        # no arguments produces an error
        self.assertEqual(self.shell.use_command.run(self.shell, []), 1)
        # both list and a plugin produces an error
        self.assertEqual(self.shell.use_command.run(self.shell, ['--list', 'plugin']), 1)

    @patch('provisioner.ui.cli.cmd.use.UseCommand.list_plugins')
    def test_list_plugins(self, mock_list_plugins: MagicMock):
        mock_list_plugins.return_value = 0

        # list can be called with -l and --list
        self.assertEqual(self.shell.use_command.run(self.shell, ['-l']), 0)
        self.assertEqual(self.shell.use_command.run(self.shell, ['--list']), 0)
        self.assertEqual(mock_list_plugins.call_count, 2)

    def test_use_plugin(self):
        # calling without a config ID results in adding without a config ID
        self.shell.ctx.job = MagicMock()
        mock_options = self.shell.ctx.job.options

        self.assertEqual(self.shell.use_command.run(self.shell, ['testplugin']), 0)
        mock_options.resolve_config.return_value.add_plugin.assert_called_with(self.test_plugin, None)
        mock_options.reset_mock()

        # calling without a plugin ID results in adding without config ID
        self.assertEqual(self.shell.use_command.run(self.shell, ['testplugin:config_id']), 0)
        mock_options.resolve_config.return_value.add_plugin.assert_called_with(self.test_plugin, 'config_id')
        mock_options.reset_mock()

    @patch('provisioner.ui.cli.cmd.use.ConfigPath')
    def test_bad_plugin_fails(self, mock_config):
        mock_config.side_effect = ValueError
        self.assertEqual(self.shell.use_command.run(self.shell, ['fakeplugin']), 2)

    def test_plugin_with_remainder_fails(self):
        self.assertEqual(self.shell.use_command.run(self.shell, ['test.plugin']), 2)

    def test_plugin_not_found_fails(self):
        self.assertEqual(self.shell.use_command.run(self.shell, ['fakeplugin']), 3)

    def test_remove_nonexistent_config(self):
        self.shell.ctx.job = MagicMock()
        mock_options = self.shell.ctx.job.options
        mock_options.del_conf.side_effect = ValueError

        # No config ID produces an error
        self.assertNotEqual(self.shell.use_command.run(self.shell, ['--delete', 'testplugin']), 0)
        self.assertEqual(mock_options.del_conf.call_count, 1)  # only called once for counting configs to delete
        mock_options.reset_mock()

    @patch('builtins.input')
    def test_remove_all_plugin_configs(self, mock_input: MagicMock):
        self.shell.ctx.job = MagicMock()
        mock_options = self.shell.ctx.job.options
        call_0 = call(self.test_plugin, 'config_0')
        call_1 = call(self.test_plugin, 'config_0')
        mock_options.del_conf.return_value = 2

        # answering 'no' DOES NOT remove plugins
        mock_input.return_value = "no"
        self.assertNotEqual(self.shell.use_command.run(self.shell, ['-d', 'testplugin']), 0)
        mock_input.called_once()
        self.assertEqual(mock_options.del_conf.call_count, 1)
        mock_options.reset_mock()

        # answering 'yes' DOES remove plugins
        mock_input.return_value = "yes"
        self.assertEqual(self.shell.use_command.run(self.shell, ['-d', 'testplugin']), 0)
        mock_input.called_once()
        mock_options.del_conf.has_calls([call_0, call_1])

        # also works with emtpy colon
        self.assertEqual(self.shell.use_command.run(self.shell, ['-d', 'testplugin:']), 0)
        mock_input.called_once()
        mock_options.del_conf.has_calls([call_0, call_1, call_0, call_1])
