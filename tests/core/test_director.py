import unittest
from unittest.mock import MagicMock, patch

from provisioner.core.director import Director

class TestDirector(unittest.TestCase):
    def setUp(self, ):
        self.plugin_store = MagicMock()
        self.job = MagicMock()
        self.database = MagicMock()
        self.director = Director(self.job, self.database, self.plugin_store)
        self.mock_plugin = MagicMock()
        self.mock_plugin.source_dir = "/fake/path"
        self.mock_plugin.usage = {
            "include_role": {
                "name": "MockPlugin",
            },
        }

    def test_no_child_plugin(self):
        mock_config = MagicMock()
        mock_child1 = MagicMock()
        mock_child1.plugin = None
        mock_child2 = MagicMock()
        mock_config.children = [mock_child1, mock_child2]

        with self.assertRaises(ValueError):
            self.director._generate_subtask(mock_config)

    def test_generate_subtask(self):
        mock_config = MagicMock()
        mock_child1 = MagicMock(name="SomeCoolExploit", path="plugins.coolstuff", children=None)
        mock_child2 = MagicMock(name="LameExploit", path="plugins.lamestuff", children=None)
        mock_child1.plugin.get_usage.return_value = {'include_role': {'name': "somecoolexploit"}}
        mock_child2.plugin.get_usage.return_value = {'include_role': {'name': "lameexploit"}}

        mock_config.children = [mock_child1, mock_child2]

        ret = self.director._generate_subtask(mock_config)
        self.assertEqual(ret, [mock_child1.plugin.get_usage.return_value, mock_child2.plugin.get_usage.return_value])

    def test_generate_subtask_children(self):
        mock_config = MagicMock()
        mock_child1 = MagicMock(name="SomeCoolExploit", path="plugins.coolstuff", children=None)
        mock_child2 = MagicMock(name="LameExploit", path="plugins.lamestuff", children=None)
        mock_child1.plugin.get_usage.return_value = {'include_role': {'name': "somecoolexploit"}}
        mock_child2.plugin.get_usage.return_value = {'include_role': {'name': "lameexploit"}}

        mock_child1.children = [mock_child2]

        mock_config.children = [mock_child1]

        ret = self.director._generate_subtask(mock_config)
        self.assertEqual(ret, [mock_child1.plugin.get_usage.return_value, mock_child2.plugin.get_usage.return_value])

    def test_plays_not_generated(self):
        mock_config = MagicMock()
        mock_child = MagicMock()
        mock_child.plugin = None
        mock_config.children = [mock_child]

        with self.assertRaises(ValueError):
            self.director.generate_plays(mock_config)

    @patch('provisioner.core.director.Director._generate_subtask')
    def test_generate_plays(self, mock_gen):
        self.maxDiff = None
        mock_config = MagicMock(options=[], defaults=[], config_id='plugins', parent=None, plugin=None)
        mock_child1 = MagicMock()
        mock_child1.path = 'lame.plugin'
        mock_child1.name = 'lameplugin'
        mock_child1.tree.return_value = {'child': 'output'}
        mock_child1.plugin.id = "lameID"
        mock_child1.plugin.usage = {"include_role": {"name": "lameplugin"}}
        mock_child1.plugin.name = "lameplugin"

        mock_config.children = [mock_child1]
        mock_gen.return_value = [{"name": "mock_subtasks"}]

        # cant really side-effect this to get unique output or i would..
        self.job.options.resolve_option.return_value.tree.return_value = "treeoutput"

        expected = [{
            'hosts': 'treeoutput', 'user': 'treeoutput', 'become': 'treeoutput', 'gather_facts': 'treeoutput',
            'become_method': 'treeoutput', 'become_user': 'treeoutput',
            'tasks': [
                {"include_role": {"name": "lameplugin"}, "name": "lameplugin - lame.plugin:lameplugin", "vars": {'child': 'output'}},
                {"name": "Register Success", "c2games.opfor.register_plugin": {"_plugin_id": "lameID"}},
                {"name": "mock_subtasks"},
            ],
            'vars': {
                'ansible_connection': 'treeoutput',
                'ansible_host_key_checking': 'treeoutput',
                'ansible_password': 'treeoutput',
                'ansible_winrm_server_cert_validation': 'validate',
                'c2games': {'event_id': 'treeoutput', 'config_id': "lame.plugin:lameplugin"}
            },
        }]

        ret = self.director.generate_plays(mock_config)
        self.assertEqual(ret, expected)
