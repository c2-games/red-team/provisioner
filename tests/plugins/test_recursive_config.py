import unittest
import json

from provisioner.structs.plugins import Plugin, RecursiveConfig
from provisioner.structs.options import Option, BoolOption, IntOption


class TestJob(unittest.TestCase):
    def setUp(self):
        altitude = IntOption(name='altitude', val=10000, oid='altitude')  # mock YML loader adding altitude

        self.plugin = Plugin(name='Test Plugin', id='test_plugin', usage={}, options=[
            Option(name='Option One', oid='option_one'),
            Option(name='Option Two', oid='option_two'),
            altitude,
        ])
        self.plugin2 = Plugin(name='Test Plugin 2', id='test_plugin2', usage={}, options=[
            Option(name='Option Three', oid='option_three'),
            BoolOption(name='Option Four', oid='option_four'),
            altitude,
        ])
        # Every yaml plugin will at least have an altitude option. Currently no PY plugins that are
        # standalone, and no current intention or engineering to do that
        self.no_options_plugin = Plugin(name='Test Plugin 3', id='test_plugin3', usage={}, options=[altitude])
    # todo test_to_from_json

    def test_config_to_from_json(self):
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.plugin2, 'test')
        config.resolve_option(f'{self.plugin2.id}:test', 'option_four').val = True
        assert config == RecursiveConfig.parse_obj(config.dict())
        assert config == RecursiveConfig.parse_raw(config.json())

    def test_parent_not_in_dict(self):
        config = RecursiveConfig(config_id='_')
        subconf = config.add_plugin(self.plugin, None)  # config id '0'
        assert 'config' not in config.dict()
        assert 'config' not in subconf.dict()

    def test_add_dupe_config(self):
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.no_options_plugin, 'first')
        with self.assertRaises(KeyError):
            config.add_plugin(self.no_options_plugin, 'first')

    def test_config_name_generation(self):
        """Test that config names are generated automatically"""
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.no_options_plugin, None)
        config.add_plugin(self.no_options_plugin, None)
        # Test that two plugins
        assert f'{self.no_options_plugin.id}:0' in config.search_configs('')
        assert f'{self.no_options_plugin.id}:1' in config.search_configs('')
        # Sanity check that we don't expect :2 to exist
        assert f'{self.no_options_plugin.id}:2' not in config.search_configs('')

    def test_get_config_ids(self):
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.plugin, None)  # config id '0'
        config.add_plugin(self.plugin, None)  # config id '1'
        config.add_plugin(self.plugin, 'test')
        config.add_plugin(self.plugin, 'blah.test.blah')
        config.add_plugin(self.plugin2, 'another.plugin')
        assert f"{self.plugin.id}:0" in config.search_configs('')
        assert f"{self.plugin.id}:1" in config.search_configs('')
        assert f"{self.plugin.id}:test" in config.search_configs('')
        assert f"{self.plugin.id}:blah.test.blah" in config.search_configs('')
        assert f"{self.plugin2.id}:another.plugin"\
               in config.search_configs('')

    def test_config_path(self):
        config = RecursiveConfig(config_id='_')
        child1 = config.add_child_config('child1')
        child2 = config.add_child_config('child2')
        child2a = config.add_child_config('child2:name')
        subchild1 = child1.add_child_config('subchild1')
        subsubchild1 = subchild1.add_child_config('subsubchild1')
        subsubsubchild1 = subsubchild1.add_child_config('subsubsubchild1')
        subsubsubchild1a = subsubchild1.add_child_config('subsubsubchild1:name1')
        subsubsubchild1b = subsubchild1.add_child_config('subsubsubchild1:name2')
        assert config.path is None
        assert child1.path == 'child1'
        assert child2.path == 'child2'
        assert child2a.path == 'child2:name'
        assert subchild1.path == 'child1.subchild1'
        assert subsubchild1.path == 'child1.subchild1.subsubchild1'
        assert subsubsubchild1.path == 'child1.subchild1.subsubchild1.subsubsubchild1'
        assert subsubsubchild1a.path == 'child1.subchild1.subsubchild1.subsubsubchild1:name1'
        assert subsubsubchild1b.path == 'child1.subchild1.subsubchild1.subsubsubchild1:name2'

    def test_top_config(self):
        config = RecursiveConfig(config_id='_')
        child1 = config.add_child_config('child1')
        child2 = config.add_child_config('child2')
        subchild1 = child1.add_child_config('subchild1')
        subsubchild1 = subchild1.add_child_config('subsubchild1')
        subsubsubchild1 = subsubchild1.add_child_config('subsubsubchild1')
        assert child1.top_config == config
        assert child2.top_config == config
        assert subchild1.top_config == config
        assert subsubchild1.top_config == config
        assert subsubsubchild1.top_config == config


    def test_mod_config_1_does_not_mod_config_2(self):
        """This checks to see that configurations are truly unique, not references to same option object"""
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.plugin, 'test')
        config.add_plugin(self.plugin, 'test2')
        config.resolve_option(f'{self.plugin.id}:test', 'option_one').val = 'value 1'
        config.resolve_option(f'{self.plugin.id}:test', 'option_two').val = 'value 2'
        assert config.resolve_option(f'{self.plugin.id}:test', 'option_one').val == 'value 1'
        assert config.resolve_option(f'{self.plugin.id}:test', 'option_two').val == 'value 2'
        config.resolve_option(f'{self.plugin.id}:test2', 'option_one').val = 'value 3'
        config.resolve_option(f'{self.plugin.id}:test2', 'option_two').val = 'value 4'
        assert config.resolve_option(f'{self.plugin.id}:test', 'option_one').val == 'value 1'
        assert config.resolve_option(f'{self.plugin.id}:test', 'option_two').val == 'value 2'
        assert config.resolve_option(f'{self.plugin.id}:test2', 'option_one').val == 'value 3'
        assert config.resolve_option(f'{self.plugin.id}:test2', 'option_two').val == 'value 4'

    def test_config_generation(self):
        """Test that all a plugins options are copied into a configuration"""
        config = RecursiveConfig(config_id='_')
        config.add_plugin(self.plugin, 'test')
        # the config should be turned into a dict of `oid`: Option pairs
        assert config.resolve_config(f'{self.plugin.id}:test').options == self.plugin.options
