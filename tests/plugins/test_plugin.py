import unittest
from pathlib import Path

import yaml

from provisioner.structs.plugins import C2GamesMeta
from provisioner.structs.options import Option, MultiOption, ListOption

MAPPING_YML = """
config_file:
  type: str
  name: Config File Path
exports:
  type: multi
  pkey:
    oid: path
    type: str
    name: Path
  options:
    hosts:
      type: multi
      pkey:
        oid: host
        type: str
        name: Host
      options:
        options:
          type: list
          name: Hosts
"""

LIST_YML = """
- oid: config_file
  type: str
  name: Config File Path
- oid: exports
  type: multi
  pkey:
    oid: path
    type: str
    name: Path
  options:
    - oid: hosts
      type: multi
      pkey:
        oid: host
        type: str
        name: Host
      options:
        - oid: options
          type: list
          name: Hosts
"""


# noinspection PyMethodMayBeStatic
# pylint: disable=missing-function-docstring,no-self-use,unused-variable,invalid-name
class PluginLoaderTests(unittest.TestCase):
    """
    PluginLoader and PluginStore test cases
    """

    def _test_options_loaded(self, plug):
        assert plug.options[0].oid == 'config_file'
        assert type(plug.options[0]) == Option
        assert plug.options[1].oid == 'exports'
        assert type(plug.options[1]) == MultiOption
        assert plug.options[1].options[0].oid == 'hosts'
        assert type(plug.options[1].options[0]) == MultiOption
        assert plug.options[1].options[0].options[0].oid == 'options'
        assert type(plug.options[1].options[0].options[0]) == ListOption

    def test_load_options_as_list(self):
        self._test_options_loaded(C2GamesMeta(
            name='test', id='test', options=yaml.safe_load(LIST_YML), source_dir=Path('.'), usage={}
        ))

    def test_load_options_as_mapping(self):
        self._test_options_loaded(C2GamesMeta(
            name='test', id='test', options=yaml.safe_load(MAPPING_YML), source_dir=Path('.'), usage={}
        ))



if __name__ == '__main__':
    unittest.main()
