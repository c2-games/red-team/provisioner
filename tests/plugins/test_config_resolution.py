import unittest

from provisioner.structs.plugins import ConfigPath

from tests.common import TEST_YAML_NFS_PLUGIN, get_base_plugin_config


class OptionResolutionTests(unittest.TestCase):
    def test_config_path_parse(self):
        path = ConfigPath('job.ansible')
        assert path.config_id == 'job'
        assert path.name is None
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name is None
        assert path.remainder.remainder is None

        path = ConfigPath('job:unit_test.ansible:inner_name')
        assert path.config_id == 'job'
        assert path.name == 'unit_test'
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name == 'inner_name'
        assert path.remainder.remainder is None

    def test_config_class_addition(self):
        path = ConfigPath('job') + ConfigPath('ansible')
        assert path.config_id == 'job'
        assert path.name is None
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name is None
        assert path.remainder.remainder is None

        path = ConfigPath('job:unit_test') + ConfigPath('ansible:inner_name')
        assert path.config_id == 'job'
        assert path.name == 'unit_test'
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name == 'inner_name'
        assert path.remainder.remainder is None

    def test_config_str_addition(self):
        path = ConfigPath('job') + 'ansible'
        assert path.config_id == 'job'
        assert path.name is None
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name is None
        assert path.remainder.remainder is None

        path = ConfigPath('job:unit_test') + 'ansible:inner_name'
        assert path.config_id == 'job'
        assert path.name == 'unit_test'
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name == 'inner_name'
        assert path.remainder.remainder is None

        path = 'job' + ConfigPath('ansible')
        assert path.config_id == 'job'
        assert path.name is None
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name is None
        assert path.remainder.remainder is None

        path = 'job:unit_test' + ConfigPath('ansible:inner_name')
        assert path.config_id == 'job'
        assert path.name == 'unit_test'
        assert path.remainder.config_id == 'ansible'
        assert path.remainder.name == 'inner_name'
        assert path.remainder.remainder is None

    def test_config_none_addition(self):
        path = ConfigPath('job') + None
        assert path.config_id == 'job'
        assert path.name is None
        assert path.remainder is None

        path = None + ConfigPath('job:unit_test')
        assert path.config_id == 'job'
        assert path.name == 'unit_test'
        assert path.remainder is None

    def test_config_list_addition(self):
        with self.assertRaises(TypeError):
            ConfigPath('job') + list()
        with self.assertRaises(TypeError):
            list() + ConfigPath('job')

    def test_config_path_equality(self):
        assert ConfigPath('job') == ConfigPath('job')
        assert ConfigPath('job.ansible') == ConfigPath('job.ansible')
        assert ConfigPath('job.ansible:test') == ConfigPath('job.ansible:test')
        assert ConfigPath('job:unit_tests.ansible:test') == ConfigPath('job:unit_tests.ansible:test')

    def test_config_path_from_config_path(self):
        path = ConfigPath('job.ansible:test')
        path2 = ConfigPath(path)
        assert path == path2

    def test_one(self):
        conf = get_base_plugin_config()
        assert len(conf.search_configs('ansible'))
        assert conf.resolve_config('ansible') is not None
        assert conf.resolve_option('ansible', 'hosts') is not None
        # Load Plugin
        nfs = TEST_YAML_NFS_PLUGIN
        plugins = conf.resolve_config('plugins')
        subconf1 = plugins.add_plugin(nfs, name='unit_test_1')  # add first nfs plugin to plugins.nfs
        subconf2 = subconf1.add_plugin(nfs, name='unit_test_2')  # add second nfs plugin to plugins.nfs.nfs
        assert conf.resolve_config('plugins.nfs.nfs') is subconf2
        assert conf.resolve_config('plugins.nfs:unit_test_1.nfs') is subconf2
        assert conf.resolve_config('plugins.nfs.nfs:unit_test_2') is subconf2
        assert conf.resolve_config('plugins.nfs:unit_test_1.nfs:unit_test_2') is subconf2

        # add a third nfs plugin to plugins.nfs.nfs
        subconf3 = subconf1.add_plugin(nfs, name='unit_test_3')
        # Retrieving without name causes a KeyError because two config_id's exist at the same level
        with self.assertRaises(KeyError):
            conf.resolve_config('plugins.nfs:unit_test_1.nfs')

        # check we can still resolve by name
        assert conf.resolve_config('plugins.nfs.nfs:unit_test_2') is subconf2
        assert conf.resolve_config('plugins.nfs.nfs:unit_test_3') is subconf3

        # technically invalid syntax, but still resolves, because python
        assert conf.resolve_config('plugins.nfs:.nfs:unit_test_3') is subconf3
        # Raises exception with invalid syntax
        with self.assertRaises(KeyError):
            conf.resolve_config('plugins.nfs:unit_test_1.nfs:')

        # double colon
        with self.assertRaises(ValueError):
            assert conf.resolve_config('plugins.nfs:unit:test.nfs:unit_test_3') is subconf3
