"""
TODO: reformat and break out tests into own modules, depending on what's being tested
"""

import unittest
import os
from pathlib import Path
from typing import Type, Union, Tuple
from unittest.mock import MagicMock

from provisioner.errors.plugins import PluginNotLoadedError
from provisioner.structs.plugins import get_plugin_name, C2GamesMeta
from provisioner.loaders import PluginLoader
from provisioner.loaders.yaml import YamlPluginLoader
from provisioner.loaders.python import PythonPluginLoader
from provisioner.structs.plugins.store import PluginStore

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
TEST_PY_DIR = os.path.join(BASE_PATH, 'python')
TEST_PY_FILE = os.path.join(BASE_PATH, 'python', 'plugin.py')
TEST_PY_NONPLUGIN_FILE = os.path.join(BASE_PATH, 'python', 'nonplugin.py')
TEST_YAML_DIR = os.path.join(BASE_PATH, 'yaml_plugs')
TEST_EMPTY_YML_FILE = os.path.join(TEST_YAML_DIR, 'empty.c2games.yml')
TEST_YML_FILE = os.path.join(TEST_YAML_DIR, 'plugin1', 'plugin.c2games.yml')
TEST_YAML_FILE = os.path.join(TEST_YAML_DIR, 'plugin2', 'plugin2.c2games.yaml')
TEST_NONEXIST_FILE = os.path.join(BASE_PATH, 'nonexist', 'plugin.py')


def get_test_loader(
        cls: Type[Union[YamlPluginLoader, PythonPluginLoader]],
        **kwargs) -> Tuple[PluginStore, PluginLoader, MagicMock]:
    """
    Initialize a PluginLoader of type CLS, setup a temporary PluginStore,
    and create a mock logging object
    :param cls: PluginLoader object to Instantiate
    :param kwargs: Extra parameters passed directly to CLS.__init__()
    :return: Tuple of PluginStore, PluginLoader and Mock Logger
    """
    logger = MagicMock()
    logger.critical = MagicMock()
    logger.error = MagicMock()
    logger.warn = MagicMock()
    logger.warning = logger.warn
    logger.info = MagicMock()
    logger.debug = MagicMock()
    logger.log = MagicMock()

    store = PluginStore(logger=logger)
    loader = cls(plugin_store=store, logger=logger, plugin_basedir=BASE_PATH, **kwargs)  # type: ignore
    return store, loader, logger


# noinspection PyMethodMayBeStatic
# pylint: disable=missing-function-docstring,no-self-use,unused-variable,invalid-name
class PluginLoaderTests(unittest.TestCase):
    """
    PluginLoader and PluginStore test cases
    """

    def test_load_py_file_as_str(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        plugin = loader.load_plugin(TEST_PY_FILE)
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        assert str(plugin.source) == TEST_PY_FILE
        assert plugin.source == Path(TEST_PY_FILE)

    def test_load_py_file_as_path(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        plugin = loader.load_plugin(Path(TEST_PY_FILE))
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        assert str(plugin.source) == TEST_PY_FILE
        assert plugin.source == Path(TEST_PY_FILE)

    def test_can_load_py_dir(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        loader.find_plugins(TEST_PY_DIR)
        assert len(store.plugins.keys()) == 2

    def test_load_empty_yaml_file(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        plugin = loader.load_plugin(TEST_EMPTY_YML_FILE)
        assert plugin is None
        assert len(store.plugins.keys()) == 0

    def test_load_yaml_file_as_str(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        plugin = loader.load_plugin(TEST_YAML_FILE)
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        assert str(plugin.source) == TEST_YAML_FILE
        assert plugin.source == Path(TEST_YAML_FILE)

    def test_load_yaml_file_as_path(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        plugin = loader.load_plugin(Path(TEST_YAML_FILE))
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        assert str(plugin.source) == TEST_YAML_FILE
        assert plugin.source == Path(TEST_YAML_FILE)

    def test_load_yml_file_as_path(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        plugin = loader.load_plugin(Path(TEST_YML_FILE))
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        assert str(plugin.source) == TEST_YML_FILE
        assert plugin.source == Path(TEST_YML_FILE)

    def test_can_load_yaml_dir(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        loader.find_plugins(TEST_YAML_DIR)
        assert len(store.plugins.keys()) == 3

    def test_dont_load_py_in_yaml(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        loader.find_plugins(TEST_PY_DIR)
        assert len(store.plugins.keys()) == 0

    def test_dont_load_yaml_in_py(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        loader.find_plugins(TEST_YAML_DIR)
        assert len(store.plugins.keys()) == 0

    def test_load_plugins_twice(self):
        store, loader, logger = get_test_loader(YamlPluginLoader, target_class=C2GamesMeta)
        loader.find_plugins(TEST_YAML_DIR)
        loader.find_plugins(TEST_YAML_DIR)
        logger.error.assert_called()

    def test_remove_plugin(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        # Load plugin
        plugin = loader.load_plugin(TEST_PY_FILE)
        assert plugin is not None
        assert len(store.plugins.keys()) == 1
        # Remove Plugin
        store.remove_plugin('python/plugin')
        assert len(store.plugins.keys()) == 0
        # Remove plugin again
        with self.assertRaises(PluginNotLoadedError):
            store.remove_plugin('python/plugin')

    def test_plugin_name_generation(self):
        assert get_plugin_name(str(Path(TEST_YAML_FILE).parent), BASE_PATH) == "yaml_plugs/plugin2"

    def test_nonexistent_name_generation(self):
        assert get_plugin_name(TEST_NONEXIST_FILE, BASE_PATH) == "nonexist/plugin"

    def test_load_non_plugin(self):
        store, loader, logger = get_test_loader(PythonPluginLoader)
        plugin = loader.load_plugin(TEST_PY_NONPLUGIN_FILE)
        assert plugin is None
        logger.debug.assert_called_with("no plugin found in module python.nonplugin")


if __name__ == '__main__':
    unittest.main()
