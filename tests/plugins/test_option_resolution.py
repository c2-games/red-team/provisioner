import unittest

from pydantic import ValidationError

from provisioner.structs.options import OptionPath, MultiOption, IntOption
from tests.common import get_base_plugin_config, TEST_YAML_NFS_PLUGIN


class TestOptionResolution(unittest.TestCase):
    def setUp(self):
         self.alt_opt = IntOption(name='altitude', val=10000, oid='altitude')  # mock YML loader adding altitude

    def test_oid_validator(self):
        path = OptionPath('hosts[192.168.1.1]')
        with self.assertRaises(ValidationError):
            path.oid = 'value.with.dots'

    def test_config_path_parse(self):
        # Test can parse IP pkey's
        path = OptionPath('hosts[192.168.1.1]')
        assert path.oid == 'hosts'
        assert path.key == '192.168.1.1'
        assert path.remainder is None

        path = OptionPath('exports.options')
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder.oid == 'options'
        assert path.remainder.key is None
        assert path.remainder.remainder is None

        path = OptionPath('exports.hosts[192.168.1.1]')
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder is None

        path = OptionPath('exports[/etc].hosts')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key is None
        assert path.remainder.remainder is None

        path = OptionPath('exports[/etc].hosts[192.168.1.1]')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder is None

        path = OptionPath('exports[/etc].hosts[192.168.1.1].options')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder.oid == 'options'
        assert path.remainder.remainder.key is None
        assert path.remainder.remainder.remainder is None

    def test_config_class_addition(self):
        path = OptionPath('exports') + OptionPath('options')
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder.oid == 'options'
        assert path.remainder.key is None
        assert path.remainder.remainder is None

        path = OptionPath('exports[/etc]') + OptionPath('hosts[192.168.1.1]')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder is None

    def test_config_str_addition(self):
        path = OptionPath('exports') + 'options'
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder.oid == 'options'
        assert path.remainder.key is None
        assert path.remainder.remainder is None

        path = OptionPath('exports[/etc]') + 'hosts[192.168.1.1]'
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder is None

        path = 'exports' + OptionPath('options')
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder.oid == 'options'
        assert path.remainder.key is None
        assert path.remainder.remainder is None

        path = 'exports[/etc]' + OptionPath('hosts[192.168.1.1]')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder.oid == 'hosts'
        assert path.remainder.key == '192.168.1.1'
        assert path.remainder.remainder is None

    def test_config_none_addition(self):
        path = OptionPath('exports') + None
        assert path.oid == 'exports'
        assert path.key is None
        assert path.remainder is None

        path = None + OptionPath('exports[/etc]')
        assert path.oid == 'exports'
        assert path.key == '/etc'
        assert path.remainder is None

    def test_config_list_addition(self):
        with self.assertRaises(TypeError):
            OptionPath('job') + list()
        with self.assertRaises(TypeError):
            list() + OptionPath('job')

    def test_config_path_equality(self):
        assert OptionPath('exports') == OptionPath('exports')
        assert OptionPath('exports.hosts') == OptionPath('exports.hosts')
        assert OptionPath('exports.hosts[192.168.1.1]') == OptionPath('exports.hosts[192.168.1.1]')
        assert OptionPath('exports[/etc].hosts[192.168.1.1]') == OptionPath('exports[/etc].hosts[192.168.1.1]')
        assert OptionPath('exports[/etc].hosts[192.168.1.1].options') ==\
               OptionPath('exports[/etc].hosts[192.168.1.1].options')

    def test_config_path_from_config_path(self):
        path = OptionPath('exports[/etc].hosts[192.168.1.1].options')
        path2 = OptionPath(path)
        assert path == path2

    def test_one(self):
        conf = get_base_plugin_config()
        # Load Plugin
        plugins = conf.resolve_config('plugins')
        # add nfs plugin to plugins.nfs
        nfs_conf = plugins.add_plugin(TEST_YAML_NFS_PLUGIN, name='unit_test_1')

        # Get some base options from the config
        conf.resolve_config('plugins.nfs')
        assert conf.resolve_option('plugins.nfs', 'config_file') is nfs_conf.options[0]
        assert conf.resolve_option('plugins.nfs', 'exports') is nfs_conf.options[1]

        # Get Exports multi-option
        exports_opt: MultiOption = conf.resolve_option('plugins.nfs', 'exports')  # type: ignore
        # Add export with primary key /etc
        etc_key, etc_opts = exports_opt.add_pkey('/etc')
        # Add host to /etc export with IP 192.168.1.1 (manually)
        host_192_168_1_1_key, host_192_168_1_1_opt = etc_opts[0].add_pkey('192.168.1.1')  # type: ignore
        # Add second export with primary key /home
        home_key, home_opts = exports_opt.add_pkey('/home')
        # Add host to /home export with IP 192.168.254.254 (via API)
        host_192_168_254_254_key, host_192_168_254_254_opt = \
            conf.resolve_option('plugins.nfs', 'exports[/home].hosts').add_pkey('192.168.254.254')  # type: ignore

        # TEST: Get exports by primary key
        assert conf.resolve_option('plugins.nfs', 'exports[/etc]') is etc_key
        assert conf.resolve_option('plugins.nfs', 'exports[/etc].hosts') is etc_opts[0]
        assert conf.resolve_option('plugins.nfs', 'exports[/home]') is home_key
        assert conf.resolve_option('plugins.nfs', 'exports[/home].hosts') is home_opts[0]
        # TEST: reverse checks, make sure we're not fibbing and using the same data for both options
        assert conf.resolve_option('plugins.nfs', 'exports[/etc]') is not home_key
        assert conf.resolve_option('plugins.nfs', 'exports[/etc].hosts') is not home_opts[0]
        assert conf.resolve_option('plugins.nfs', 'exports[/home]') is not etc_key
        assert conf.resolve_option('plugins.nfs', 'exports[/home].hosts') is not etc_opts[0]

        # TEST: Get host 192.168.1.1 from /etc export
        assert conf.resolve_option('plugins.nfs', 'exports[/etc].hosts[192.168.1.1]') is host_192_168_1_1_key
        # TEST: Get host OPTIONS for 192.168.1.1 from /etc export
        assert conf.resolve_option('plugins.nfs', 'exports[/etc].hosts[192.168.1.1].options') is host_192_168_1_1_opt[0]
        # TEST: Add to host Options for 192.168.1.1 from /etc export, ensure the values are added
        original = conf.resolve_option('plugins.nfs', 'exports[/etc].hosts[192.168.1.1].options').val.copy()  # type: ignore
        original.append('TEST')
        conf.resolve_option('plugins.nfs', 'exports[/etc].hosts[192.168.1.1].options').append('TEST')  # type: ignore
        assert original == conf.resolve_option('plugins.nfs', 'exports[/etc].hosts[192.168.1.1].options').val
        # TEST: Other host option was not modified
        assert host_192_168_254_254_opt[0].val == host_192_168_254_254_opt[0].default_val()
        assert host_192_168_254_254_opt[0].val != host_192_168_1_1_opt[0].val

        # Some exception testing, ensure we don't accidentally pull a value given a non-existent key
        with self.assertRaises(KeyError):
            conf.resolve_config('NONEXIST')
        with self.assertRaises(KeyError):
            conf.resolve_config('NONEXIST[/etc]')
        with self.assertRaises(KeyError):
            conf.resolve_config('exports[NONEXIST]')
        with self.assertRaises(KeyError):
            conf.resolve_config('exports[/etc].NONEXIST')
        with self.assertRaises(KeyError):
            conf.resolve_config('exports[/etc].NONEXIST[192.168.1.1]')
        with self.assertRaises(KeyError):
            conf.resolve_config('exports[/etc].hosts[NONEXIST]')
        with self.assertRaises(KeyError):
            conf.resolve_config('exports[/etc].hosts[192.168.1.1].NONEXIST')













