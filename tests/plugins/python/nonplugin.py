from typing import List

from provisioner.structs.plugins import Plugin


class InitD(Plugin):
    provides: List[str] = ['persistence', 'linux.persistence']
