import unittest
from unittest.mock import patch, MagicMock

from provisioner.cli import main


class TestCliStub(unittest.TestCase):
    @patch('provisioner.ui.cli.shell.MongoDB')
    @patch('sys.exit')
    @patch('provisioner.ui.cli.shell.ProvisionerShell.cmdloop')
    def test_shell_starts(self, mock_shell: MagicMock, mock_exit: MagicMock, mock_mongo: MagicMock):
        mock_shell.return_value = 0
        main()
        mock_shell.assert_called_once()
        mock_exit.assert_called_with(0)

