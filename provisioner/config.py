from cincoconfig.config import Schema
from cincoconfig.fields import (
    FilenameField,
    HostnameField,
    ListField,
    LogLevelField,
    PortField,
    StringField,
    SecureField,
)


# Define programmatically how conf.yml is structured
PROVISIONER_SCHEMA = Schema(env='C2GAMES')
PROVISIONER_SCHEMA.log_level_file = LogLevelField(default="debug")
PROVISIONER_SCHEMA.log_level_console = LogLevelField(default="info")
PROVISIONER_SCHEMA.log_file = FilenameField(default="provisioner.log")
PROVISIONER_SCHEMA.plugins_path = StringField()
PROVISIONER_SCHEMA.plugins_path_ignore = StringField()
PROVISIONER_SCHEMA.event_id = StringField()
PROVISIONER_SCHEMA.database.database = StringField(default="c2games")
PROVISIONER_SCHEMA.database.host = HostnameField(default="localhost")
PROVISIONER_SCHEMA.database.port = PortField(default=27017)
PROVISIONER_SCHEMA.database.ssl_cert = FilenameField()
PROVISIONER_SCHEMA.database.user = StringField()
PROVISIONER_SCHEMA.database.password = SecureField()

# PROVISIONER_SCHEMA.template = [
#   {
#       "name": "test",
#       "vmid": "117",
#       "os_type": "ubuntu"
#   },
PROVISIONER_SCHEMA.template = ListField(default=[])
