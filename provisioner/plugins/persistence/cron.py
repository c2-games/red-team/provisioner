from typing import List, Dict

from pydantic import Field

from provisioner.structs.plugins import Plugin
from provisioner.structs.options import (
    Option, CronTimeMinuteOption, CronTimeHourOption,
    CronTimeDayOption, CronTimeMonthOption, CronTimeDOTWOption, CronFileOption,
    AnyOption
)


class CronMixin(Plugin):
    """
    Plugin for persistence with Cron jobs
    """
    provides: List[str] = ['persistence', 'linux.persistence']
    usage: Dict[str, Dict] = Field(default_factory=lambda: {'c2games.opfor.persistence': {"cron": {}}})
    options: List[AnyOption] = Field(default=[
        Option(
            oid='cwd', name="Working Directory",
            description="Working Directory to execute cron Job from"
        ),
        CronFileOption(
            oid='cron_file', name="Cron File",
            description="If specified, uses this file instead of an individual user's crontab."
                        "If this is a relative path, it is interpreted with respect to /etc/cron.d."
                        "If it is absolute, it will typically be /etc/crontab."
                        "Many linux distros expect (and some require) the filename portion to consist solely of "
                        "upper- and lower-case letters, digits, underscores, and hyphens."
                        "To use the cron_file parameter you must specify the user as well."
        ),
        Option(
            oid='special_time', name="Special Time",
            description="Special time specification nickname (ex, 'hourly')"
        ),
        Option(
            oid='user', name="User", default_val="root",
            description="The specific user whose crontab should be modified."
                        "When unset, this parameter defaults to the current user."
        ),
        CronTimeMinuteOption(
            oid='minute', name="Minute", description="Minute when the job should run"
        ),
        CronTimeHourOption(
            oid='hour', name="Hour", description="Hour when the job should run"
        ),
        CronTimeDayOption(
            oid='day', name="Day", description="Day of the month when the job should run"
        ),
        CronTimeMonthOption(
            oid='month', name="Month", description="Month of the year when the job should run",
        ),
        CronTimeDOTWOption(
            oid='day_of_week', name="Day of the Week", description="Day of the week when the job should run",
        )
    ])

    def get_usage(self, name, task_vars):
        """
        Configure the name and vars to what the ansible cron module will be looking
        to accept
        """
        tmp = super().get_usage(name, task_vars)
        tmp['c2games.opfor.persistence']['cron']['name'] = name
        for key, value in task_vars.items():
            tmp['c2games.opfor.persistence']['cron'][key] = value

        return tmp


__plugin__ = CronMixin
