from __future__ import annotations
import sys
from typing import List, TYPE_CHECKING

from pydantic import NoneStr
from pypsi.core import PypsiArgParser, CommandShortCircuit
from pypsi.format import Table

from provisioner.structs.plugins import Plugin, ConfigPath
from provisioner.structs.plugins.store import PluginStore
from provisioner.ui.cli.cmd import ProvisionerCommand
if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class UseCommand(ProvisionerCommand):
    """
    Use a loaded plugin in the current loadout
    """
    def __init__(self, name='use', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Use a loaded plugin in the current loadout",
        )

        self.parser.add_argument(
            '-l', '--list', help='list all plugins', action='store_true'
        )

        self.parser.add_argument(
            '-d', '--delete', help='remove plugin configuration', action='store_true'
        )

        self.parser.add_argument(
            '-f', '--force', help='force removal of all configurations with without prompting', action='store_true'
        )

        self.parser.add_argument(
            '-p', '--parent', default='plugins', completer=self.complete_configuration_ids,
            help='parent configuration path for the plugin to nest under'
        )

        self.parser.add_argument(dest='plugin', metavar='plugin', nargs='?', completer=self._complete_plugins)
        super().__init__(name=name, brief="select a plugin to load into a Job", **kwargs)

    def _complete_plugins(self, shell: ProvisionerShell, args: List[str], prefix: str):
        if '-d' in args or '--delete' in args and not prefix.startswith('-'):
            return self.complete_configuration_ids(shell, args, prefix)
        return self.complete_plugin_names(shell, args, prefix)

    # pylint: disable=arguments-differ
    def run(self, shell: ProvisionerShell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
            if not args.list and not args.plugin:
                shell.error("the following arguments are required: PLUGIN")
                return 1
            elif args.list and args.plugin:
                shell.error("cannot specify --list and PLUGIN at the same time")
                return 1
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        if args.list:
            return self.list_plugins(shell)

        try:
            path = ConfigPath(args.plugin)
        except ValueError as e:
            shell.error(f"Invalid plugin ID: {e}")
            return 2

        if args.delete:
            return self.delete_plugins(shell, path, force=args.force)

        if path.remainder:
            shell.error("Invalid plugin ID: a remainder was found."
                        " Ensure your plugin ID does not contain a period ('.')")
            return 2

        if path.config_id not in shell.ctx.plugin_store.plugins:
            shell.error(f'plugin ID not found: {path.config_id}')
            return 3

        plugin = shell.ctx.plugin_store.plugins[path.config_id]

        return self.use_plugins(shell, path, plugin, args.parent)

    def use_plugins(self, shell: ProvisionerShell, path: ConfigPath, plugin: Plugin, parent: NoneStr = None) -> int:
        """
        Use a plugin in the current loadout
        """
        config = shell.ctx.job.options
        try:
            subconf = config.resolve_config(parent)
            conf = subconf.add_plugin(plugin, path.name)
        except Exception as e:
            shell.error(f"failed to use plugin: {e}")
            return 4
        print(f'plugin now in use at: {conf.path}')
        return 0

    def delete_plugins(self, shell: ProvisionerShell, path: ConfigPath, force=False) -> int:
        """
        Delete a plugin configuration from the job. If config_id=None,
        all configurations for that plugin will be removed.

        :param shell: Provisioner Shell
        :param plugin: Plugin Object to remove
        :param config_id: Configuration ID to remove.
        :param force: Force removal of multiple configurations
        :return:
        """
        config = shell.ctx.job.options
        try:
            count = config.del_conf(path, what_if=True)
        except Exception as e:
            shell.error(f'failed to delete configurations: {e}')
            return 4

        if count > 1 and not force:
            delete = 'yes' == input(f"WARNING! No configuration ID was specified. This command will "
                                    f"delete {count} configurations. Type 'yes' to continue: ")
        else:
            delete = True

        if not delete:
            shell.error(f"answer was not 'yes', not removing {count} configurations")
            return 5

        try:
            count = config.del_conf(path, what_if=False)
        except Exception as e:
            shell.error(f'failed to delete configurations: {e}')
            return 6

        print(f'successfully removed {count} configurations')
        return 0

    def list_plugins(self, shell: ProvisionerShell) -> int:
        """
        Print plugins available to screen
        """
        # todo this prints looking horrible for some reason :(
        table = Table(3, width=shell.width, header=True)
        plugin_store: PluginStore = shell.ctx.plugin_store
        # noinspection PyTypeChecker
        table.append('Plugin ID', 'Plugin Name', 'Plugin Source')  # add table headers
        for name, plugin in plugin_store.plugins.items():
            table.append(plugin.id, name, plugin.source)

        # table.append("\n")
        # table.append('Loaded Plugins ID', 'Configuration ID')
        # for ids in shell.ctx.job.options.resolve_config('plugins'):
        #     table.append(*self.parse_plugin_id(ids))

        table.write(sys.stdout)
        return 0
