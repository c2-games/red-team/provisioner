# pylint: disable=too-many-return-statements, too-many-branches
from __future__ import annotations
from typing import List, Any, TYPE_CHECKING, Iterable

from pydantic import ValidationError
from pypsi.core import PypsiArgParser, CommandShortCircuit

from provisioner.ui.cli.cmd import ProvisionerCommand
from provisioner.structs.options import Option, MultiOption, AnyOption

if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class SetCommand(ProvisionerCommand):
    """
    Set an <Option> value for a <Job> or plugin
    """

    def __init__(self, name='set', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Set options for a task or job",
        )

        # noinspection PyTypeChecker
        self.parser.add_argument(
            'option', completer=self.complete_oid_path,
            help='The option to configure',
        )

        self.parser.add_argument(
            'value', help='Value to set the option to', nargs='*', completer=self.complete_opt_values
        )

        self.parser.add_argument(
            '-R', '--reset', help='Reset the option to its default value', action='store_true'
        )

        self.parser.add_argument(
            '-a', '--add', action='store_true', help="Append the given values to the given option."
        )

        self.parser.add_argument(
            '-r', '--remove', action='store_true', help="Remove the given values to the given option."
        )

        self.parser.add_argument(
            '-s', '--show', action='store_true', default=False, help="Show the value of a given option",
        )

        super().__init__(name=name, brief="configure Option/Job/Ansible variables in the current Job", **kwargs)

    def complete_opt_values(self, shell: ProvisionerShell, raw_args: List[str], _) -> Iterable[str]:
        """Complete Possible Option Values, if available"""
        # Create a mini parser to get the first positional arg
        parser = PypsiArgParser(prog='tmp')
        parser.add_argument('option')
        args, _ = parser.parse_known_args(raw_args)

        try:
            # parse the option path
            cid, oid = self.parse_config_path(args.option)

            # get the option handle
            handle: AnyOption = shell.ctx.job.options.resolve_option(cid, oid)

            # if choices, return them
            return set(str(c).lower() for c in getattr(handle, 'choices', []) if c is not None)
        except KeyError:
            return ""

    # pylint: disable=arguments-differ
    def run(self, shell: ProvisionerShell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        try:
            # Either of these will raise if given invalid input
            cid, oid = self.parse_config_path(args.option)
            handle = shell.ctx.job.options.resolve_option(cid, oid)
        except (ValueError, KeyError) as e:
            shell.error(e)
            return 1

        try:
            if args.reset:
                handle.reset()
                return 0

            if args.show:
                print(handle.tree())
                return 0

            if not args.value:
                shell.error('no value specified!')
                return 2

            if args.add:
                try:
                    handle += args.value
                except TypeError:
                    shell.error('option does not support --add')
                    return 7
                return 0

            if args.remove:
                return self.remove_option(handle, args.value)

            try:
                value = args.value if len(args.value) > 1 else args.value[0]
                if isinstance(handle, MultiOption):
                    handle.add_pkey(value)
                elif isinstance(handle, Option):  # elif for typing
                    handle.val = value
            except ValidationError as e:
                # Print nested errors from the error wrappers
                for err in e.raw_errors:
                    shell.error(err.exc)  # type: ignore
                return 4
            return 0
        except ValueError as exc:
            shell.error(exc)
            return 1

    def remove_option(self, option: AnyOption, values: List[Any]) -> int:
        """
        Remove the given values from an option
        """
        for value in values:
            try:
                print(f"Deleting {value} from option")
                del option[value]  # type: ignore
            except KeyError as exc:
                raise ValueError(f"Error occurred: {option.oid} did not have {value}") from exc
            except (AttributeError, TypeError) as exc:
                raise ValueError(f"Error occurred: {option.oid} does not support item deletion") from exc

        return 0
