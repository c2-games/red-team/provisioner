from __future__ import annotations
import sys
from typing import List, TYPE_CHECKING

from pypsi.core import PypsiArgParser, CommandShortCircuit
from pypsi.format import Table, Column

from provisioner.structs.plugins.store import PluginStore
from provisioner.ui.cli.cmd import ProvisionerCommand
from provisioner.structs.options import MultiOption

if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class ShowCommand(ProvisionerCommand):
    """
    Display details about job, plugins, options, etc.
    """
    def __init__(self, name='show', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Show details about jobs, plugins, options, etc.",
        )

        subcmd = self.parser.add_subparsers(dest="topic")
        subcmd.required = True

        subcmd.add_parser("job", help="List information about the current job")
        subcmd.add_parser("options", help="List current job and plugin options")
        plugins = subcmd.add_parser("plugins", help="List information about specific or all plugins in plugin store")
        plugins.add_argument(
            dest="item_id", nargs='?', completer=self.complete_oid_path, help="Specific plugin to show details about.",
        )

        super().__init__(name=name, brief="display details about the job/plugins/options", **kwargs)

    def complete(self, shell, args, prefix):
        if len(args) == 1:
            return [x for x in ('job', 'options', 'plugins')
                    if x.startswith(prefix)]

        if len(args) >= 2:
            if args[0] == 'plugins':
                return self.complete_plugin_names(shell, args, prefix)
        return []

    # pylint: disable=arguments-differ
    def run(self, shell: ProvisionerShell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        if args.topic == "plugins":
            if args.item_id:
                return self.plugin_info(shell, args.item_id)
            else:
                return self.show_plugins(shell)
        if args.topic == "job":
            return self.show_job(shell)
        if args.topic == "options":
            return self.show_options(shell)

        return 1

    def show_options(self, shell: ProvisionerShell) -> int:
        """
        Show all currently configured job and plugin options and their values
        """
        # Write out the job/ansible options
        table = Table([Column("Config"), Column("Value")], width=shell.width, header=True)
        configs = [config for config in shell.ctx.job.options.children if len(config.options) >= 1]
        for config in configs:
            for option in config.options:
                table.append(f"{config.path}!{option.oid}", str(getattr(option, 'val', 'N/A')))

            table.append('', '')

        # Write out any options configured from currently loaded plugins
        for plugin in shell.ctx.job.options.resolve_config('plugins').children:
            plug_name = f"plugins.{plugin.config_id}:{plugin.name}"
            options = plugin.options

            for option in options:
                name = f"{plug_name}!{option.oid}"
                if isinstance(option, MultiOption):
                    # TODO: see if we can make this prettier
                    table.append(name, option.tree())
                else:
                    table.append(name, getattr(option, 'val', 'N/A'))

            table.append('', '')

        table.write(sys.stdout)

        return 0

    def show_job(self, shell: ProvisionerShell) -> int:
        """
        Print job configuration and meta information
        """
        table = Table([Column("Job Metadata"), Column("Value")], width=shell.width, header=True)
        table.append("Event ID", shell.ctx.job.event_id)
        table.append("Job ID", shell.ctx.job.job_id)
        table.append('', '')
        table.write(sys.stdout)

        table = Table([Column("Config"), Column("Value")], width=shell.width, header=True)
        config = shell.ctx.job.options.resolve_config('job')
        for option in config.options:
            table.append(f"job!{option.oid}", str(getattr(option, 'val')))

        table.append('', '')
        table.write(sys.stdout)

        table = Table([Column("Loaded Plugins"), Column("Altitude")], width=shell.width, header=True)
        plugins = list(shell.ctx.job.options.resolve_config('plugins').children)
        plugins.sort(key=lambda x: x.option_from_name('altitude').val)  # type: ignore

        for plugin in plugins:
            table.append(f"plugins.{plugin.config_id}:{plugin.name}",
                         plugin.option_from_name('altitude').val)  # type:ignore

        table.write(sys.stdout)

        return 0

    def plugin_info(self, shell: ProvisionerShell, plugin_id: str) -> int:
        """
        Show more details about a specific plugin and information on its configurable options
        """
        table = Table([
            Column("Plugin ID"), Column("Difficulty"), Column("Description")],
            width=shell.width, header=True)
        plugin_store: PluginStore = shell.ctx.plugin_store

        plugin = plugin_store.plugins.get(plugin_id)
        if not plugin:
            table.append(f"Plugin {plugin_id} does not exist!", "N/A", "N/A")
            table.write(sys.stdout)
            return 1
        else:
            name = plugin.id
            diff = getattr(plugin, 'difficulty', 'N/A')
            info = getattr(plugin, 'description', 'N/A')
            table.append(name, diff, info)

            # New table with new column structure
            table = Table([Column("Option ID"), Column("description")], width=shell.width, header=True)

            for option in shell.ctx.plugin_store.plugins[plugin_id].options:
                if isinstance(option, MultiOption):
                    table.append(option.oid, "Multioption")  # TODO: multioption fix with recursion
                else:
                    table.append(option.oid, getattr(option, 'description', 'N/A'))

            table.write(sys.stdout)

        return 0

    def show_plugins(self, shell: ProvisionerShell) -> int:
        """
        Print all or specific plugin to screen
        """
        table = Table([Column("Plugin ID"), Column("Difficulty"), Column("Description")],
                      width=shell.width, header=True)
        plugin_store: PluginStore = shell.ctx.plugin_store

        for plugin in plugin_store.plugins.values():
            name = plugin.id
            diff = getattr(plugin, 'difficulty', 'N/A')
            info = getattr(plugin, 'description', 'N/A')
            table.append(name, diff, info)

        table.write(sys.stdout)
        return 0
