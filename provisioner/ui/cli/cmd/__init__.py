from __future__ import annotations
from abc import ABC
from dataclasses import dataclass
from typing import List, Optional, Tuple, TYPE_CHECKING, Union

from pypsi.completers import command_completer
from pypsi.core import Command, PypsiArgParser
from pypsi.shell import Shell

from provisioner.structs.plugins import RecursiveConfig, ConfigPath
from provisioner.core.mongodb import MongoDB
from provisioner.structs.plugins import Plugin
from provisioner.structs.options import OptionPath

if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class ProvisionerCommand(Command, ABC):
    """
    Base Command class implement a couple common functions
    """
    def __init__(self, *args, **kwargs):
        if not getattr(self, 'parser', None):
            self.parser = PypsiArgParser()
        super().__init__(*args, **kwargs)

    def run(self, shell: ProvisionerShell, args: List[str]) -> int:
        '''
        Execute the command. All commands need to implement this method.

        :param pypsi.shell.Shell shell: the active shell
        :param list args: list of string arguments
        :returns int: 0 on success, less than 0 on error, and greater than 0 on
            invalid usage
        '''
        raise NotImplementedError()

    def complete(self, shell: Shell, args: List[str], prefix: str):
        """
        Return tab completions of:
         1. Arguments defined with PypsiArgParser
         2. Positional Parameters with a completer function
         3. Flag Parameter Values with a completer function
        """
        try:
            return command_completer(self.parser, shell, args, prefix)
        except Exception as e:
            shell.error(e)

    @staticmethod
    def complete_jobs(shell, args, _):
        """
        Complete Plugin Names
        """
        mongo: MongoDB = shell.ctx.db
        cursor = mongo.db.jobs.find()
        if not cursor:
            return []
        ret = []
        for obj in cursor:
            if obj.get('name') and obj['name'].startswith(args[-1]):
                ret.append(obj['name'])
        return ret

    @staticmethod
    def complete_plugin_names(shell, args, _):
        """
        Complete Plugin Names
        """
        return [name for name in shell.ctx.plugin_store.plugins.keys() if name.startswith(args[-1])]

    @staticmethod
    def complete_configuration_ids(shell, _, prefix):
        """
        Complete plugin & Configuration IDs
        """
        return shell.ctx.job.options.search_configs(prefix)

    def complete_oid_path(self, shell, _, prefix):
        """
        List the config IDs and option IDs available
        """
        c_prefix, _ = self.parse_config_path(prefix)
        config_ids: List[str] = shell.ctx.job.options.search_configs(c_prefix)
        opts = []
        for cid in config_ids:
            config: RecursiveConfig = shell.ctx.job.options.resolve_config(cid)
            for opt in config.options:
                opts.extend([f'{cid}!{comp}' for comp in opt.get_completions()])
        return opts

    @staticmethod
    def parse_config_path(path: str) -> Tuple[Union[ConfigPath, None], Union[OptionPath, None]]:
        """
        Parse a full path into a separate ConfigPath and OptionPath, separated by a '!'
        """
        config_id, oid = path.split('!', 1) if '!' in path else [path, None]  # type: ignore
        if oid and '!' in oid:
            raise ValueError("Invalid Config Path: Multiple Option Separators ('!') detected")
        c_path = ConfigPath(config_id) if config_id else None
        o_path = OptionPath(oid) if oid else None
        return c_path, o_path
