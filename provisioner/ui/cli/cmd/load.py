from pathlib import Path
from typing import List, Union

from pypsi.core import PypsiArgParser, CommandShortCircuit
from pypsi.completers import path_completer
from pypsi.shell import Shell

from provisioner.loaders import PluginLoader
from provisioner.loaders.yaml import YamlPluginLoader
from provisioner.loaders.python import PythonPluginLoader
from provisioner.ui.cli.cmd import ProvisionerCommand
from provisioner.utils.format import shorten_home_path


class LoadCommand(ProvisionerCommand):
    """
    Load, reload or unload a plugin from disk
    """

    def __init__(self, name='load', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Use a loaded plugin in the current loadout",
        )

        # noinspection PyTypeChecker
        self.parser.add_argument(
            'path', help='path to plugin file or directory',
            completer=lambda shell, args, prefix: path_completer(prefix, prefix)
        )

        self.parser.add_argument(
            '-r', '--recursive', help='Recursively load a directory of plugin files',
            action='store_true'  # todo make exclusive of name
        )

        self.parser.add_argument(
            '-R', '--remove', help='Unload a plugin ID from the global plugin store',
            action='store_true'
        )

        self.parser.add_argument(
            '-P', '--python', help='Load python files', action='store_true'
        )

        # todo make yaml/python mutually exclusive in parser
        self.parser.add_argument(
            '-Y', '--yaml', help='Load yaml files as Quirks', action='store_true'
        )

        # todo allow specifying name to load plugin as
        # todo don't allow loading existing plugins
        # self.parser.add_argument(
        #     '-R', '--reload', help='Reload plugin'
        # )

        # self.subcmd = self.parser.add_subparsers()
        # name = self.subcmd.add_parser('name')
        # name.add_argument(
        #     '-n', '--plugin-name', help='Name to import plugin as'
        # )

        # name = self.parser.add_mutually_exclusive_group()
        # name.add_argument(
        #     '-n', '--plugin-name', help='Name to import plugin as'
        # )

        super().__init__(name=name, brief="load plugins from disk into the provisioner", **kwargs)

    def complete(self, shell: Shell, args: List[str], prefix: str):
        """
        Return tab completion matches
        """
        if '-R' in args or '--remove' in args:
            return self.complete_plugin_names(shell, args, prefix)
        return super().complete(shell, args, prefix)

    # pylint: disable=arguments-differ
    def run(self, shell: Shell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        if args.yaml and args.python:
            shell.error('cannot load python and yaml plugins at the same time')
            return 1
        if args.remove:
            return self.unload_plugin(shell, args.path)
        return self.load_plugins(shell, args.path, args.recursive, python=args.python, yaml=args.yaml)

    def unload_plugin(self, shell: Shell, plugin_id: str) -> int:
        """
        Unload a plugin from the global plugin store
        """
        if plugin_id not in shell.ctx.plugin_store.plugins:
            shell.error("plugin not found")
            return 3
        del shell.ctx.plugin_store.plugins[plugin_id]
        return 0

    def load_plugins(self, shell: Shell, path: Union[Path, str], recursive=False,
                     name=None, python=False, yaml=False) -> int:
        """
        Load a plugin(s) into the global plugin store
        """
        path = Path(path)  # ensure we have a Path object
        ldr: PluginLoader
        if yaml or list(filter(str(path).endswith, YamlPluginLoader.PLUGIN_SUFFIXES)):
            ldr = YamlPluginLoader(plugin_store=shell.ctx.plugin_store, plugin_basedir=path)
            plug_type = 'YAML'
        elif python or path.suffix in PythonPluginLoader.PLUGIN_SUFFIXES:
            ldr = PythonPluginLoader(plugin_store=shell.ctx.plugin_store, plugin_basedir=path)
            plug_type = 'Python'
        else:
            shell.error('must load either python or yaml files')
            return 2

        if not path.exists():
            shell.error(f'path not found: {path}')
            return 3

        num_plugs = len(shell.ctx.plugin_store.plugins)

        if recursive:
            ldr.find_plugins(path)
        else:
            ldr.load_plugin(path, name=name)

        path = shorten_home_path(path)

        print(f'loaded {len(shell.ctx.plugin_store.plugins) - num_plugs} {plug_type} plugin(s) from {path}')
        return 0
