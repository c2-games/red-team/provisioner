from enum import Enum
from typing import Dict

from bson import ObjectId
from cincoconfig import Config as CincoConfig
from pydantic import Field, validator, root_validator

from provisioner.structs.pydantic import BaseModel
from provisioner.utils.decorators import exclude_fields
from provisioner.utils.validators import value_is_object_id
from provisioner.deploy import SiteDeployment
from provisioner.structs.plugins import RecursiveConfig
from provisioner.structs.options import Option, TargetList, BoolOption, IntOption, AnsibleHostsOption


class JobStatus(Enum):
    """
    Status of a <Job>

    * Unknown: Job is in an unknown state.
    * Pending: Job has been initialized
    * Running: Job is being executed
    * Successful: Job was executed with no errors
    * Failed: Job was executed with errors
    """
    Unknown = "unknown"
    Pending = "pending"
    Running = "running"
    Successful = "successful"
    Failed = "failed"


class Job(BaseModel):
    """
    The Job class holds the configuration information for executing a set of tasks
    """

    #: SiteDeployment.Config object - CincoConfig object is initialized CincoConfig Schema - aka, Schema(...)()
    config: CincoConfig = Field(default_factory=lambda: SiteDeployment.config)
    options: RecursiveConfig = Field(default=None)
    event_id: str = 'default_event'
    job_id: ObjectId = Field(default_factory=ObjectId)
    status: JobStatus = JobStatus.Pending

    @root_validator(pre=True)
    def _set_default_options(cls, values):  # pylint: disable=no-self-argument
        """
        Ensure Base Options Configuration is set. Must be a Validator
        because config is required for get_base_configuration()
        """
        if not values.get('options'):
            values['options'] = cls.get_base_configuration(values['config'])
        return values

    @validator('status', pre=True, always=True)
    def status_is_enum(cls, val):  # pylint: disable=no-self-argument
        """Ensure the Value is a JobStatus Enum."""
        return JobStatus(val)

    _job_id_is_object_id = validator('job_id', pre=True)(value_is_object_id)

    @property
    def done(self):
        """
        Boolean property, whether or not the job has hit a finished state

        Returns:
            True: if status is successful or failed
            False: otherwise
        """
        return self.status in (JobStatus.Successful.value, JobStatus.Failed.value)

    def check(self) -> Dict[str, str]:
        """
        Iterate over all job and plugin options and determine if every option is valid
        """
        # todo test and finish configuration checks
        return self.options.check(self)  # type: ignore

    @exclude_fields('config')
    def dict(self, *args, **kwargs):
        """Override the dict function to exclude the config attribute."""
        return super().dict(*args, **kwargs)

    @staticmethod  # unused arg here will be used by a branch in the future
    def get_base_configuration(config: CincoConfig) -> RecursiveConfig:  # pylint: disable=unused-argument
        """
        Return an initialized RecursiveConfig used as the base for all jobs
        :param config: Application Configuration
        :return: Initialized RecursiveConfig
        """
        options = RecursiveConfig(config_id='_', name='_')
        options.add_child_config(config_id='plugins')
        options.add_child_config(config_id='defaults')
        options.add_child_config(config_id='job', options=[
            TargetList(oid='targets', name="Job Targets", required=True),
            Option(oid='event_id', name='Event ID', default=lambda: getattr(config, 'event_id', 'default_event')),
        ])
        options.add_child_config(config_id='ansible', options=[
            Option(name="Password", oid="password"),
            Option(name="User", oid="user", default_val="ansible", required=True),
            BoolOption(name="Become", oid="become", default_val="yes"),
            Option(name="Become User", oid="become_user", default_val="root"),
            Option(name="Become Password", oid="become_password"),
            Option(name="Become Method", oid="become_method", default_val="sudo"),
            BoolOption(name="Gather Facts", oid="gather_facts", default_val=True),
            Option(name="Ansible Key", oid="ssh_key", default_val="~/.ssh/ansible_key"),
            IntOption(name="Ansible Verbosity", oid='verbosity', choices=range(0, 5)),
            AnsibleHostsOption(name="Hosts", oid="hosts", default_val=["all"], required=True),
            Option(name="Connection Type", oid="connection", default_val="ssh"),
            Option(name="Host Certificate Validation", oid="host_cert_validation", default_val=False),
        ])
        return options
