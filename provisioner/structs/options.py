# pylint: disable=no-self-argument
from __future__ import annotations

import logging
from copy import deepcopy
from copy import copy
from enum import Enum
import re
from typing import List, Any, Optional, Union, Dict, TYPE_CHECKING, Type, Tuple, ClassVar, Callable, Match

# TODO: Unit test all the options!!
# TODO Path and LocalPath options (local path for local system, like Ansible SSH key)
from pydantic import NoneStr, Field, validator, root_validator, validate_model

from provisioner.structs.pydantic import BaseModel
from provisioner.utils.validators import (
    coerce_oid_from_options, oid_contains_no_dots, validate_net_addr, values_in_choices
)

if TYPE_CHECKING:
    from provisioner.structs.plugins import RecursiveConfig
    from provisioner.core.jobs import Job


logger = logging.getLogger(__name__)


class OptionPath(BaseModel):
    """
    Helper class to parse Option Paths. OptionPaths consist of three parts:
    oid[key].remainder

    The oid represents the Option ID, is the only required value, and cannot contain '.'
    The key represents the value of the primary key to look up
    The remainder represents another OptionPath
    """

    oid: str
    key: NoneStr
    remainder: Optional[OptionPath]
    # First match group ensures:
    #  1. There are no periods
    #  2. There is no open brace '['
    #  ([^\.\[]*)
    # Second match group makes the entire [key] bracket optional, and is thrown away
    # (\[([^\]] *)\])?
    # Third match group is inside the second, and matches the key itself
    # The key may have any character except close-brace ']'
    # ([^\]]*)
    # An optional period is allowed before the fourth match group, which is entire remainder (no validation)
    # .?(.*)
    _PATH_REGEX = re.compile(r'([^.\[]*)(\[([^]]*)])?.?(.*)')

    _normalize_oid = validator('oid', pre=True, always=True, allow_reuse=True)(oid_contains_no_dots)

    def __init__(self, path: OptionPathNoneStr):
        oid, key, remainder = self.parse_path(str(path or ''))  # Convert to empty string if None
        super().__init__(oid=oid, key=key, remainder=OptionPath(remainder) if remainder else None)

    @classmethod
    def parse_path(cls, path: str) -> Tuple[str, NoneStr, NoneStr]:
        """
        Parse a path into the config_id, name, and remainder
        :param path: Path to parse
        :return: Tuple of config_id, Optional[name], and Optional[remainder]
        """
        result: Optional[Match[str]] = cls._PATH_REGEX.search(path)
        if not result:
            raise ValueError("Could not parse OptionPath")
        oid, _, key, remainder = result.groups()
        # This code has a bug where a key with a period in it (like an IP address)
        # will cause it to split incorrectly. Keeping it here as a starting point
        # for when _PATH_REGEX fails us
        # this, remainder = path.split('.', 1)
        # match = cls._KEY_REGEX.search(this)
        # oid, key = match.groups() if match else [this, None]
        # if match and match.end() != match.endpos:
        #     raise ValueError(f"Option Key had trailing characters: {path} (bad='{match.string[match.end():]}'")
        return oid, key, remainder

    def __str__(self):
        """
        Create a string representing the OptionPath as:
            f"{oid}[{key}].{remainder}"
        """
        ret = self.oid
        if self.key:
            ret = f'{ret}[{self.key}]'
        if self.remainder:
            ret = f'{ret}.{self.remainder}'
        return ret

    def __add__(self, other):
        """
        Add two OptionPath's together
        :param other: Another OptionPath or String that will be parsed as an Option Path
        :return: Combined OptionPath
        """
        if isinstance(other, (str, OptionPath)):
            return OptionPath(f"{str(self)}.{str(other)}")
        elif other is None:
            return self
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")

    def __radd__(self, other):
        """
        Add two OptionPath's together (with a string on the left)
        :param other: Another OptionPath or String that will be parsed as an Option Path
        :return: Combined OptionPath
        """
        if isinstance(other, (str, OptionPath)):
            return OptionPath(f"{str(other)}.{str(self)}")
        elif other is None:
            return self
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")


class AnyOption(BaseModel):
    """Abstract Option Class"""
    op_type: ClassVar[str] = Field(alias='type')
    oid: str

    _normalize_oid = validator('oid', pre=True, always=True, allow_reuse=True)(oid_contains_no_dots)

    def __new__(cls, **kwargs):
        """Automatically Initialize an Option to it's appropriate type."""
        # If Class is already determined, do nothing special
        if cls is not AnyOption:
            return super(AnyOption, cls).__new__(cls)

        # We don't know the appropriate class yet
        # Try to get it from op_type, otherwise assume it's just Option
        op_type = kwargs.get('op_type', kwargs.get('type', 'str'))

        # Retrieve the appropriate class
        clazz = OPTION_MAP.get(op_type)
        if not clazz:
            # Uh-oh, class not found! Possibly invalid YAML
            raise ValueError(f"Invalid option type: {op_type}")

        # initialize the class we determined
        # python will handle calling __init__ as appropriate
        return super(AnyOption, clazz).__new__(clazz)  # type: ignore

    def __call__(self, **kwargs) -> AnyOption:
        """
        Create a clone of the Option class when called, to allow storing instantiated versions of the object with oid
        and other data set
        :param **kwargs: Attributes to set on the object
        :return Option: Copy of self
        """
        ret = deepcopy(self)
        for k, v in kwargs.items():
            if hasattr(ret, k):
                setattr(ret, k, v)
        return ret

    @classmethod
    def from_json(cls, data, oid=None):
        """
        Convert a python dict into an initialized option. If `type` is available, it will be checked against
        the known Option types and that option type will be loaded
        :param data: Data to convert into an Option class
        :param oid: Optionally specify the Option ID separate from Data. Overrides the attribute in data, if present
        :return Option: Initialized option class
        """
        # todo make this a custom json.loads function?
        op_type = data.pop('type', 'str')
        clazz = OPTION_MAP.get(op_type)
        if not clazz:
            msg = f"Invalid option type: {op_type}"
            if oid:
                msg = f"{oid}: {msg}"
            raise ValueError(msg)

        if oid:
            data['oid'] = oid

        try:
            return clazz(**data)
        except Exception as e:
            raise ValueError(f'invalid option "{data.get("oid", "(unknown ID)")}": ({type(e)}) {e}') from e

    def get_option(self, path: Union[OptionPath, str]) -> AnyOption:
        """
        Returns self on most options, or will return a sub-option on MultiOptions
        :param path: Period (.) delimited path to desired option
        :raises ValueError: Error when a remainder exists in the path because this is not a MultiOption class
        """
        path = OptionPath(path)  # ensure it's an OptionPath
        if path.remainder or path.key:
            raise ValueError(f'A remainder or key exists but this is not a MultiOption! path={path} ({self})')
        return self

    # pylint: disable=unused-argument
    def check(self, job: Job, configuration: RecursiveConfig) -> List[str]:
        """
        Check that an Option can validate it's values, and more complex checks (like dependencies) are met
        :param job: Current Job Object
        :param configuration: Current level of RecursiveConfig
        :return: List of errors, or empty list if all good
        """
        return []

    def tree(self, by_var=False):
        """Get the Option value or Value Tree"""
        raise NotImplementedError()

    def from_tree(self, value: Any, _parent: str = None):
        """
        Convert the values back from tree format. Used when loading a saved job
        :param value: Value as it is returned from self.tree()
        :param _parent: Internal, parent oid for printing warning/error
        """
        raise NotImplementedError()

    @property
    def display_name(self):
        """Retrieve Display Name for Option"""
        raise NotImplementedError()

    def reset(self):
        """Reset an Option to it's Default Value"""
        raise NotImplementedError()

    def __repr__(self):
        """String Representation of Option"""
        return f'{type(self)}(oid={self.oid},value={self.tree()})'

    def dict(self, *args, **kwargs):
        """Override pydantic dict() method to convert default_val to it's value"""
        ret = super().dict(*args, **kwargs)
        # Special handling around default because it is a callable() function
        if 'default' in ret and callable(ret['default']):
            ret['default'] = ret['default']()
        if 'default_val' in ret and callable(ret['default_val']):
            ret['default_val'] = ret['default_val']()
        # Add op_type, because ClassVar's are excluded by default, but we want it
        ret['op_type'] = self.op_type
        return ret

    def get_completions(self) -> List[str]:
        """Return Possible Option Paths"""
        return [self.oid]


class MultiOption(AnyOption):
    """
    MultiOptions are Options which map a Primary Key Option to a list of other Options or MultiOption. Each Primary Key
    _must_ be an Option Class, and _can not_ be a MultiOption. The Options contained by the MultiOption can be normal
    Options or additional MultiOptions.

    MultiOptions are used by creating a new Primay Key with `add_pkey(value)`. An instance of the pkey Option class will
    be created with a val of `value`. The pkey Option will be added as a hashed key to the `items` dict, then all the
    sub-options from `options` will be copied into the dict.
    """
    op_type = 'multi'
    oid: str
    pkey: Option = Field(allow_mutation=False)
    options: List[AnyOption] = Field(allow_mutation=False)
    items: Dict[Option, List[AnyOption]] = Field(default_factory=dict)

    @validator('items', pre=True)
    def initialize_item_keys(cls, value, values):
        """Ensure that the item keys are initialized PKey Instances."""
        ret = {}
        pkey = values['pkey']
        for key, opts in value.items():
            key = key if isinstance(key, AnyOption) else pkey(val=key)
            ret[key] = opts
        return ret

    @validator('pkey', pre=True, always=True, allow_reuse=True)
    def pkey_is_cls(cls, new):
        """Convert the Primary Key to an Option Class"""
        return cls._validate_option('Primary Key', new)

    _coerce_oid_from_options = validator('options', pre=True, allow_reuse=True)(coerce_oid_from_options)

    @validator('options', pre=True, always=True, allow_reuse=True)
    def options_are_cls(cls, new):
        """Convert the dicts in the Options List to Option Classes"""
        return [cls._validate_option('MultiOption Option', op) for op in new]

    @classmethod
    def _validate_option(cls, cls_name, new):
        """
        Convert a dict to an Option Class
        :param cls_name: Class name for Exceptions raised
        :param new: Dictionary of Values
        :return: Instantiated Option Class
        """
        if not new:
            raise ValueError(f'{cls_name} is required')

        if isinstance(new, AnyOption):
            return new

        return AnyOption(**new)

    @property
    def display_name(self):
        return self.pkey.name

    def __getitem__(self, pkey) -> List[AnyOption]:
        """
        Lookup a set of Options based on the `pkey` value
        :param pkey: Bare Primary Key Value (NOT the Option class itself)
        :return: List of Options associated with the Primary Key
        """
        _, opts = self._lookup(pkey)
        if opts:
            return opts
        raise KeyError(f'Option PKey value not found: {pkey}')

    def __delitem__(self, pkey):
        """
        Delete a set of Options based on the `pkey` value
        :param pkey: Bare Primary Key Value (NOT the Option class itself)
        """
        for key in self.items:
            if key.val == pkey:
                del self.items[key]
                return

        raise KeyError(f'Option PKey value not found: {pkey}')

    def _lookup(self, pkey) -> Tuple[Optional[Option], Optional[List[AnyOption]]]:
        """
        Lookup a set of Options based on the `pkey` value
        :param pkey: Bare Primary Key Value (NOT the Option class itself)
        :return Tuple: A tuple containing the Primary Key Option and the associated List of Options
        """
        for key in self.items:
            if key.val == pkey:
                return key, self.items[key]
        return None, None

    def remove(self, pkey):
        """
        Interface to delete a set of Options based on the 'pkey' value
        :param pkey: Bare Primary Key Value (NOT the Option class itself)
        """
        del self[pkey]

    def add_pkey(self, pkey: Any) -> Tuple[Option, List[AnyOption]]:
        """
        Add a new primary key and option set to the MultiOption
        :param pkey: Primary Key value
        :return: Tuple of Primary Key Option and associated Option List
        """
        key, _ = self._lookup(pkey)
        if key:
            # todo should we deduplicate on value here? Is there a benefit? Will we ever want duplicate keys?
            raise ValueError("Pkey is already in MultiOption!")

        if not pkey.__hash__:
            raise ValueError(f"The pkey, {pkey} is not hashable!")

        try:
            pkey = self.pkey(val=pkey)
        except Exception as e:
            raise TypeError(f"invalid primary key value: {e}") from e
        # Re-instantiate all the options stored into a new list
        self.items[pkey] = [op() for op in self.options]
        return pkey, self.items[pkey]

    def get_option(self, path: Union[OptionPath, str]) -> AnyOption:
        """
        Get a sub-option from the MultiOption
        :param path: Period (.) delimited path to desired option
        :return AnyOption: Option or MultiOption matching the desired path
        :raises KeyError: If primary key is invalid or not found
        """
        path = OptionPath(path)  # ensure it's an OptionPath
        if path.oid == self.oid and not path.key and not path.remainder:
            # This is a special case, where we are passed our own OID and no key or remainder.
            # This means we were actually the final destination - there is no more path to resolve
            return self
        # Get the List of Options relating to the pkey defined in the path
        key, options = self._lookup(path.key)
        if not key or options is None:
            raise KeyError(f'Option PKey value not found: {path}')
        if not path.remainder:
            # If there is not a remainder, we want to return just the primary key
            # Ex, "plugins.nfs!exports[/etc]" should return the path Option with value '/etc'
            return key

        # If there is a remainder, we'll keep digging down
        for opt in options:
            if opt.oid == path.remainder.oid:
                # Call the remainder's `get_option()` function. If we received the string:
                #    exports[/etc].hosts[192.168.1.1].options
                # Then the child option with oid `hosts` will receive:
                #    hosts[192.168.1.1].options
                # This sends the `hosts` oid to the `hosts` option, but this is fine because `get_option` is designed
                # to ignore the top level `path.oid` is receives (and only searches for the top level `path.key`).
                #
                # If the result is a simple option, like the oid `.options` in the example, the option
                # will return itself from `get_option`, so long as a key and remainder are not defined.
                return opt.get_option(path.remainder)
        raise KeyError(f'Remainder oid not found: {path} ({path.remainder.oid})')

    def tree(self, by_var=False):
        """Return a map of {OID: Values} ready for use by the plugin."""
        ret = {}
        for pkey, opts in self.items.items():
            # Return primaryKey: OptionMap, so there is content to what the MultiOptions items are. Ex,
            # {
            #   'exports': {    # Exports MultiOption
            #     '/etc': {     # Path PrimaryKey
            #       'hosts': {  # Hosts MultiOption
            #         '*': {    # Host Value
            #           'options': ['no_squash_root']  # Options ListOption
            # }}}}}
            ret[pkey.val] = {getattr(opt, 'var', opt.oid) if by_var else opt.oid: opt.tree() for opt in opts}
        return ret

    def from_tree(self, value: Dict, _parent: str = None):
        """
        Convert the values back from tree format. Used when loading a saved job
        :param value: Value as it is returned from self.tree()
        :param _parent: Internal, parent oid for printing warning/error
        """
        for pkey, items in value.items():
            self.add_pkey(pkey)
            for oid, opt_value in items.items():
                try:
                    self.get_option(f"[{pkey}].{oid}").from_tree(opt_value, _parent=_parent + oid if _parent else oid)
                except Exception as e:
                    logger.debug(f"failed to load value in from_tree: {e}")
                    print(f'failed to set value for previous oid '
                          f'{f"{_parent}." if _parent else ""}[{pkey}].{oid} - value was: {opt_value}')

    def check(self, job, configuration) -> List[str]:
        """
        :return:
        """
        ret = []
        for opt in self.options:
            errors = opt.check(job, configuration)
            if errors:
                ret.extend(errors)
        return ret

    def get_completions(self) -> List[str]:
        ret = [self.oid]
        for pkey, opts in self.items.items():
            for opt in opts:
                ret.extend([f'{self.oid}[{pkey.val}].{comp}' for comp in opt.get_completions()])
        return ret

    def reset(self):
        """Delete all configured options"""
        self.items = {}

    def dict(self, *args, **kwargs):
        """Override pydantic dict() method to convert default_val to it's value"""
        ret = super().dict(*args, **kwargs)
        # Convert keys to string representation
        ret['items'] = {key.val: val for key, val in ret['items'].items()}
        return ret


class Option(AnyOption):
    """
    Base Option class to define a parameter to a plugin
    """
    op_type = 'str'
    oid: str
    name: str
    var: NoneStr = None
    description: NoneStr = None
    required: bool = False
    choices: Optional[Union[List, range]] = Field(default_factory=list)
    # Pydnatic reserves `default` and `value` as internal fields on BaseModel,
    # so we are forced to use `default_val` and `val` here
    default_val: Callable[[], Any] = Field(alias='default', default=None)
    val: Any = Field(alias='value', default=None)

    def __hash__(self):
        return hash(self.val)

    @root_validator(allow_reuse=True, skip_on_failure=True)
    def validate_choices(cls, values: Any) -> Optional[Dict[str, Any]]:
        """
        Function that validates the <_value> against allowed choices
        :raises ValidationError: Validation error containing a str description of the error
        """
        if isinstance(values, AnyOption):
            # TODO I have no idea why we need to do this, or why it works.
            #      When we create an option class using MultiOption._validate_option,
            #      an instantiated object is sent to this function instead of a dictionary
            #      of values, which causes errors to occur. Returning without Validation
            #      seems to bandaid the problem.
            return values  # type: ignore
        # Check that Value is a valid choice, if it is set
        value = values['val']
        choices = values.get('choices', [])
        oid = values.get('oid', "")
        cron_ops = ['minute', 'hour', 'day']

        # TODO cleanup cron choices to accept choices + */choices strings
        if isinstance(value, (list, set)):
            for val in value:
                if val is not None and choices:
                    cron_choices = [f'*/{choice}' for choice in choices]
                    if val not in choices:
                        if oid in cron_ops and val not in cron_choices:
                            raise ValueError(f"{val} is not one of: {choices}")

        else:
            if value is not None and choices:
                cron_choices = [f'*/{choice}' for choice in choices]
                if value not in choices:
                    if oid in cron_ops and value not in cron_choices:
                        raise ValueError(f"{value} is not one of: {choices}")
        return values

    @validator('var', always=True, allow_reuse=True)
    def var_or_oid(cls, new, values):
        """Set the default var value to the oid"""
        # todo make this safer
        return new or values.get('oid')

    @validator('default_val', pre=True, always=True, check_fields=False, allow_reuse=True)
    def default_is_a_func(cls, new):
        """Ensure that default is a function"""
        return new if callable(new) else lambda: copy(new)

    @validator('default_val', allow_reuse=True)
    def default_in_choices(cls, new, values):
        """Ensure that default is in choices, if defined"""
        choices = values.get('choices')
        val = new()
        if not choices:
            return new

        if val is None or val == []:
            return new

        if val not in choices:
            raise ValueError(f"Default of {val} is not one of: {choices}")

        return new

    @validator('default_val', allow_reuse=True)
    def default_is_valid(cls, new, values):
        """Check that default value is valid"""
        val = new()
        if val is None:
            return new

        tmp = dict(cls.__dict__)
        field = tmp['__fields__']['val']
        _, errors_ = field.validate(val, values, loc=field.alias, cls=cls)
        if errors_:
            raise ValueError(f"Default value '{new}' is not a valid value: {errors_.exc}")

        return new

    @validator('val', always=True, allow_reuse=True)
    def value_or_default(cls, new, values):
        """Set val to the default if new is None"""
        # todo call cls.validate or something, don't trust that value!
        if new is not None:
            return new
        elif values.get('default_val'):
            return values['default_val']()
        else:
            raise ValueError('val was not set and not default_val() is not available')

    @validator('choices', pre=True, allow_reuse=True)
    def choices_is_not_none(cls, new):
        """Enfoce choices as a empty list when None"""
        if new is None:
            return []
        return new

    @validator('choices', pre=True, allow_reuse=True)
    def choices_is_list(cls, new):
        """Choices must be a list of values"""
        if not isinstance(new, list) and not isinstance(new, range):
            raise ValueError(f"{new} is not a list of choices")
        return new

    @validator('choices', allow_reuse=True)
    def choices_are_valid(cls, new, values):
        """Check that every value in choices is valid"""
        tmp = dict(cls.__dict__)
        field = tmp['__fields__']['val']
        for val in new:
            _, errors_ = field.validate(val, values, loc=field.alias, cls=cls)
            if errors_:
                raise ValueError(f"value of '{val}' in choices is not a valid value: {errors_.exc}")

        return new

    def tree(self, by_var=False):
        """Override tree() method to return the option value"""
        return self.val

    def from_tree(self, value: Any, _parent: str = None):
        """
        Convert the values back from tree format. Used when loading a saved job
        :param value: Value as it is returned from self.tree()
        :param _parent: Internal, parent oid for printing warning/error
        """
        self.val = value

    @property
    def display_name(self):
        return self.name

    def reset(self):
        """Reset to default value"""
        self.val = self.default_val()

    # pylint: disable=unused-argument
    def check(self, job: Job, configuration: RecursiveConfig) -> List[str]:
        """
        Function that checks the option for complex situations;
        to be overridden and defined by subclasses

        :param Job job: Job object being checked
        :param dict configuration: Current configuration being checked
        Returns:
            str - Error string of why the value cant be set
        """
        if self.required and self.val is None:
            return [f"{self.oid}: Option is required and not set"]

        *_, val_error = validate_model(self.__class__, self.__dict__)
        if val_error:
            return [str(val_error)]

        return []


class IntOption(Option):
    """
    <Option> that represents an integer value
    """
    op_type = 'int'

    def __int__(self) -> int:
        return self.val

    @validator('val', pre=True)
    def val_is_int(cls, new):
        """Convert non-None values to integers"""
        if new is not None:
            return int(new)
        return new


class BoolOption(Option):
    """
    <Option> that represents a truthy value: yes/no, True/False
    """
    op_type = 'bool'
    # Override default values for choices
    choices: Optional[Union[List, range]] = Field(
        default_factory=lambda: ['y', 'yes', 'true', True, 'n', 'no', 'false', False]
    )
    default_val: Callable[[], Any] = Field(alias='default', default=lambda: False)

    @validator('val', allow_reuse=True)
    def val_is_bool(cls, new):
        """
        New value must either be truthy string or bool
        """
        if isinstance(new, str):
            new = str(new).lower()
            if new in ('yes', 'y', 'true'):
                return True
            elif new in ('no', 'n', 'false'):
                return False
            else:
                raise ValueError(f'Unknown boolean value: {new}')
        elif isinstance(new, bool):
            return new
        else:
            raise ValueError(f"{new} is not a string or bool!")


class _IterOption(Option):
    """Common functions for set and list options."""

    _values_in_choices = validator('val', pre=True, always=True, allow_reuse=True)(values_in_choices)

    def __iter__(self):
        """
        Allows for: `for target in plugin.targets:`
        """
        return iter(self.val)

    def __delitem__(self, val):
        """
        Delete the specified item from {self.val}
        """
        self.val.remove(val)

    @validator('val', pre=True, always=True, check_fields=False, allow_reuse=True)
    def value_or_default(cls, new, values):
        """
        Override value_or_default to handle a case where
        we're sent a list/set containing a single None value
        """
        return new if new not in (None, [None], {None}) else values['default_val']()


class ListOption(_IterOption):
    """
    Generic List option
    """
    op_type = 'list'
    default_val: Callable[[], List[Any]] = Field(default_factory=list, alias='default')

    @validator('val', pre=True, always=True, check_fields=False, allow_reuse=True)
    def verify_list(cls, new):
        """
        Ensure the new value is a list
        :param new: List of items or single new item
        :raises ValueError: When a value is invalid
        """
        if new is not None and not isinstance(new, list):
            return [new]
        return new

    def __iadd__(self, other):
        if isinstance(other, (set, list)):
            self.extend(other)
        else:
            self.append(other)
        return self

    def append(self, new):
        """
        Append to a list. Setting to a new List value so validators get run
        :param new: List of items to append
        :raises ValueError: When the values in new are invalid
        """
        tmp = list(self.val)
        tmp.append(new)
        self.val = tmp

    def extend(self, new):
        """
        Extend a list. Setting to a new List value so validators get run
        :param new: List of items to append
        :raises ValueError: When the values in new are invalid
        """
        tmp = list(self.val)
        tmp.extend(new)
        self.val = tmp


class SetOption(_IterOption):
    """
    Generic Set option
    """
    op_type = 'set'
    default_val: Callable[[], Any] = Field(alias='default', default_factory=set)

    @validator('val', pre=True)
    def _list_to_set(cls, val):
        """
        Convert val from list to set
        """
        if isinstance(val, list):
            return set(val)
        return val

    @validator('val', pre=True, always=True, allow_reuse=True)
    def correctly_expand_string(cls, new):
        """If a string is assigned to val, expand it so new='thing' becomes {'thing'} and not
        {'t', 'h', 'i', 'n', 'g'}"""
        if isinstance(new, str):
            return {new}
        return new

    @validator('val', pre=True, always=True, check_fields=False, allow_reuse=True)
    def verify_set(cls, new):
        """
        Ensure the new value is a set
        :param new: Set of items or single new item
        :raises ValueError: When a value is invalid
        """
        if new is not None and not isinstance(new, set):
            return set(new)
        return new

    @validator('default_val', pre=True)
    def _default_to_list(cls, new):
        """
        ensure default_val is set() not list()
        """
        val = new()
        if isinstance(val, list):
            return lambda: set(val)
        return new

    def __iadd__(self, other):
        if isinstance(other, (set, list)):
            self.update(other)
        else:
            self.add(other)
        return self

    def add(self, new):
        """
        Add element to set. Setting to a new set value so validators get run
        :param new: Item to add
        :raises ValueError: When the value in new is invalid
        """
        tmp = set(self.val)
        tmp.add(new)
        self.val = tmp

    def update(self, new: Union[set, List]):
        """
        Update a set with union of itself and <new>. Setting to a new set value so validators get run
        :param new: List or single element to add to set
        :raises ValueError: When the values in new are invalid
        """
        tmp = set(self.val)
        tmp.update(new)
        self.val = tmp


class TargetList(SetOption):
    """
    List of hosts to target against. Accepts hostname, IPv4, IPv6
    """
    op_type = 'target_list'
    _normalize_val = validator('val', allow_reuse=True)(validate_net_addr)


class HostOption(Option):
    """
    Host option - Checks that value is a valid hostname or IP address
    """
    op_type = 'host'
    _normalize_val = validator('val', allow_reuse=True)(validate_net_addr)


class PortOption(IntOption):
    """
    <Option> configuring a port for quirks to connect to or listen for connections on
    """
    op_type = 'port'
    privileged_ok: bool = False
    choices: Union[List, range] = Field(default_factory=lambda: range(1, 65536))

    @validator('val', allow_reuse=True)
    def validate_port_range(cls, new):
        """
        Ensure value is within range of port values
        :param new: New port num. Int type
        """
        if new is None:
            return new
        if not 0 < new <= 65535:
            raise ValueError(f"{new} is not a valid port")
        return new

    @root_validator(allow_reuse=True)
    def validate_port_priv(cls, values):
        """
        Ensure privilieged port range not used when priv is False
        :param new: New port num.
        """
        value = values.get('val')
        priv = values.get('privileged_ok')
        if value is None:
            return values
        if not priv and value <= 1024:
            raise ValueError(f"{value} is a privileged port and privileged_ok is set to {priv}")
        return values


class ErrorAction(Option):
    """
    <Option> configuring what a quirk should do upon error
    """
    op_type = 'err_action'

    class ErrorActionEnum(Enum):
        """
        Enum class for the different acceptable types of Error Actions to take
        if an error occurs
        """
        Stop = "stop"
        SilentlyContinue = "silently_continue"
        Inquire = "inquire"
        Default = Stop


#
# Special Ansible and Plugin Options - usually these just define a special check method
#
class AnsibleHostsOption(ListOption):
    """Special Ansible Hosts option"""
    op_type = 'ansible_hosts'

    def check(self, job, configuration) -> List[str]:
        """Check that Ansible Hosts doesn't contain "all" and other targets"""
        err = super().check(job, configuration)
        if err:
            return err

        # Perform checks on ansible related options
        if len(self.val) > 1 and 'all' in self.val:
            return [f"'all' cannot be used with other values {self.val}"]
        return []


class CronTimeOption(Option):
    """Special Cron Time Option"""
    op_type = "cron_time"

    @validator('val', allow_reuse=True)
    def cast_to_string_if_possible(cls, new):
        """Ensure that val is a string type"""
        if new is None:
            return new

        if not isinstance(new, (str, int)) or isinstance(new, bool):
            raise ValueError(f"Value: {new} is type {type(new)} and not int/str")

        try:
            new = int(new)
        except:
            pass

        return new

    @validator('val', allow_reuse=True)
    def convert_glob_to_none(cls, new):
        """If val is set to only *, convert to None for ansible"""
        if new == '*':
            return None
        return new

    @validator('val', allow_reuse=True)
    def enforce_glob_range(cls, new, values):
        """If glob range is given, validate denominator value against cron name type. Ex: */60 is valid for
        cron time relating to minutes, but not cron time relating to hours"""
        if new in ('*', None) or isinstance(new, int):
            return new

        if '*' not in new:
            return new

        value = int(new.split('/')[-1])
        if value in values['choices']:
            return new

        raise ValueError(f"Value: {value} not in {values['choices']})")

    @validator('val', allow_reuse=True)
    def enforce_non_glob_range(cls, new, values):
        """If normal value given, validate against cron name type. Ex: 60 is invalid for
        cron time relating to minutes, but 0-59 is valid"""
        if new is None or isinstance(new, int) or '*' in new:
            return new

        name = values['name'].lower()
        if name == 'minute':  # 0-59
            lower = 0
            upper = 60
        elif name == 'hour':  # 0-23
            lower = 0
            upper = 24
        elif name == 'day':  # 1-31
            lower = 1
            upper = 32
        elif name in ('month', 'day of the week'):
            return new
        else:
            raise ValueError(f"Error enforcing glob range {new} with range type {name}")

        value = int(new)
        if value in range(lower, upper):
            return new

        raise ValueError(f"Value: {value} not in range({lower}, {upper})")

    @validator('val', allow_reuse=True)
    def ensure_month_values(cls, new, values):
        """For month cron types, ensure a valid month is given if not a glob"""
        name = values['name'].lower()
        if name != 'month':
            return new
        if isinstance(new, int):
            raise ValueError(f"Value: {new} not valid month")
        if new is None or '*' in new:
            return new

        if new not in ['jan', 'feb', "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]:
            raise ValueError(f"Value: {new} not glob and not valid month")

        return new

    @validator('val', allow_reuse=True)
    def ensure_day_values(cls, new, values):
        """For day of the week cron types, ensure a valid day is given if not a glob"""
        name = values['name'].lower()
        if name != 'day of the week':
            return new
        if isinstance(new, int):
            raise ValueError(f"Value: {new} not valid day of the week")
        if new is None or '*' in new:
            return new

        if new not in ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']:
            raise ValueError(f"Value: {new} not glob and not day of the week")

        return new


class CronTimeMinuteOption(CronTimeOption):
    """Special Cron Time Option"""
    op_type = "cron_time_min"
    choices: range = range(0, 60)  # 0-59


class CronTimeHourOption(CronTimeOption):
    """Special Cron Time Option"""
    op_type = "cron_time_hour"
    choices: range = range(0, 24)  # 0-23


class CronTimeDayOption(CronTimeOption):
    """Special Cron Time Option"""
    op_type = "cron_time_day"
    choices: range = range(1, 32)  # 1-31


class CronTimeMonthOption(CronTimeOption):
    """Special Cron Time Option"""
    op_type = "cron_time_month"
    choices: list = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']


class CronTimeDOTWOption(CronTimeOption):
    """Special Cron Time Option"""
    op_type = "cron_time_dotw"
    choices: list = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']


class CronFileOption(Option):
    """Special Cron File Option"""
    op_type = 'cron_file'

    def check(self, job: Job, configuration: RecursiveConfig) -> List[str]:
        """Check that cron.user is set if cron.cron_file is set"""
        err = super().check(job, configuration)
        if err:
            return err

        # todo this is broken
        # user: Optional[Option] = configuration.get('persistence.cron.user')
        # if self.value and not (user and user.value):
        #     return [f"persistence.cron.user option is required when using {self.oid}"]
        return []


__ALL__: List[Type[AnyOption]] = [
    AnyOption,
    MultiOption,
    Option,
    BoolOption,
    IntOption,
    ListOption,
    SetOption,
    TargetList,
    HostOption,
    PortOption,
    ErrorAction,
    AnsibleHostsOption,
    CronTimeOption,
    CronTimeMinuteOption,
    CronTimeHourOption,
    CronTimeDayOption,
    CronTimeMonthOption,
    CronTimeDOTWOption,
    CronFileOption,
]

# isinstance(clazz, type) here checks if we're dealing with a class (and not a Type[])
# https://stackoverflow.com/questions/395735/how-to-check-whether-a-variable-is-a-class-or-not
OPTION_MAP = {
    clazz.op_type: clazz for clazz in __ALL__ if isinstance(clazz, type) and issubclass(clazz, (Option, MultiOption))
}

OptionPathStr = Union[OptionPath, str]
OptionPathNoneStr = Union[OptionPath, str, None]
OptionPath.update_forward_refs()
MultiOption.update_forward_refs()
