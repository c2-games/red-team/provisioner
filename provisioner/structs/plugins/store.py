from __future__ import annotations  # Allow returning class type from class method
import logging
from pathlib import Path

from types import ModuleType
from typing import List, Dict, Optional, Tuple, Union

from provisioner.errors.plugins import PluginNotLoadedError, PluginAlreadyExistsError
from provisioner.structs.plugins import Plugin, C2GamesMeta


class PluginStore:
    """
    In-memory storage of Plugin classes and helper functions
    """
    def __init__(self, logger=None):
        """
        Set up Plugin Store
        :param logger: Logger to print messages with
        """
        self.logger = logger or logging.getLogger(__name__)
        self.plugins: Dict[str, Union[Plugin, C2GamesMeta]] = {
            # 'Name': clazz
        }
        self.providers: Dict[str, List[Plugin]] = {
            # 'dependency': [ clazz ]
        }

    def remove_plugin(self, name: str) -> None:
        """
        Remove a plugin from the plugin store
        :param name: Plugin name used to store the plugin
        :return:
        """
        if name not in self.plugins:
            raise PluginNotLoadedError("plugin not loaded")
        clazz = self.plugins[name]
        for _, clazzes in self.providers.items():
            if clazz in clazzes:
                clazzes.remove(clazz)

        del self.plugins[name]

    def add_plugin(self, plugin_id: str, clazz: Plugin, provides: List[str]) -> None:
        """
        Add a new plugin class to the PluginStore
        :param plugin_id: Name to store the plugin as
        :param clazz: Instance of Plugin class
        :param provides: List of strings identifying what dependencies the plugin can provide
        """
        if plugin_id in self.plugins:
            raise PluginAlreadyExistsError(f"Plugin name already registered! {plugin_id}")

        dotted_name = plugin_id.replace('/', '.')
        if dotted_name not in provides:
            provides.append(dotted_name)

        self.plugins[plugin_id] = clazz

        for dep in provides:
            if dep not in self.providers:
                self.providers[dep] = []
            self.providers[dep].append(clazz)

    def add_plugin_from_module(self, mod: ModuleType, name: str) -> Tuple[str, ModuleType, Optional[Plugin]]:
        """
        Add a plugin from a module
        :param mod:
        :param name:
        :return: Tuple of Module name used, Module class, and Plugin Itself
        """
        plugin = getattr(mod, '__plugin__', None)
        if not plugin:
            self.logger.debug(f"no plugin found in module {mod.__name__}")
            return name, mod, plugin

        # Set source file of plugin
        plugin = plugin(
            name=name, id=name,
            source=Path(mod.__file__) if getattr(mod, '__file__') else None  # type: ignore[arg-type]
        )
        self.add_plugin(name, plugin, provides=plugin.provides or [])

        return name, mod, plugin

    def __iter__(self):
        """Iterate over loaded plugins"""
        return iter(self.plugins)

    def __str__(self) -> str:
        """
        Print string form of PluginStore
        """
        return f"PluginStore containing {len(self.plugins)} Plugins"  # pragma: no cover
