from __future__ import annotations
import os
import logging
from pathlib import Path
from typing import List, Optional, Dict, Any, TYPE_CHECKING, Union, Tuple
from copy import deepcopy

from pydantic import NoneStr, Field, validator

from provisioner.structs.pydantic import BaseModel
from provisioner.utils.decorators import exclude_fields
from provisioner.utils.validators import coerce_oid_from_options, options_are_instantiated
from provisioner.structs.options import AnyOption, OptionPath, OptionPathNoneStr

logger = logging.getLogger(__name__)

if TYPE_CHECKING:
    from provisioner.structs.plugins.store import PluginStore
    from provisioner.core.jobs import Job


def get_plugin_name(path: str, basedir: Union[Path, str]) -> str:
    """
    Return a plugin path to store in `PluginStore`, as is relative
    to the basedir/ directory and without a file extension.

    Ex, provisioner/plugins/persistence/at.py -> plugins/persistence/at

    :param path: File path plugin
    :param basedir: File path plugin container
    :return: Plugin Path
    """
    # Get absolute path to file, if possible
    full_path = os.path.abspath(path)
    if not os.path.exists(full_path):
        logger.warning(f'could not resolve absolute path for {path}, generating name with path as is')
        full_path = path
    rel_path = os.path.relpath(full_path, basedir)  # Get relative path from package root
    name = os.path.splitext(rel_path)[0].replace(os.path.sep, '/')
    logger.debug(f'converted "{path}" to "{name}"')
    return name


class ConfigPath(BaseModel):
    """
    Helper class to parse Configuration Paths. ConfigPaths consist of three parts:
    config_id[key].remainder

    The config_id represents the Config ID, is the only required value, and cannot contain '.'
    The key represents the value of the primary key to look up
    The remainder represents another ConfigPath
    """
    # todo validation decorators
    config_id: str
    name: NoneStr
    remainder: Optional[ConfigPath]

    def __init__(self, path: ConfigPathNoneStr):
        config_id, name, remainder = self.parse_path(str(path or ''))  # Convert to empty string if None
        super().__init__(config_id=config_id, name=name, remainder=ConfigPath(remainder) if remainder else None)

    @staticmethod
    def parse_path(path: str) -> Tuple[str, NoneStr, NoneStr]:
        """
        Parse a path into the config_id, name, and remainder
        :param path: Path to parse
        :return: Tuple of config_id, Optional[name], and Optional[remainder]
        """
        # plugins.ssh_authorized_keys:0 -> plugins, ssh_authorized_keys:0
        this, remainder = path.split('.', 1) if '.' in path else [path, None]  # type: ignore

        # plugins -> plugins, None
        config_id, name = this.split(':', 1) if ':' in this else [this, None]  # type: ignore
        if name and ':' in name:
            raise ValueError("Configuration Name contains too many ':' colons!")

        # plugins, None, ssh_authorized_keys:0
        return config_id, name, remainder

    def __str__(self):
        """
        Create a string representing the ConfigPath as:
            f"{config_id}[{key}].{remainder}"
        """
        ret = self.config_id
        if self.name:
            ret = f'{ret}:{self.name}'
        if self.remainder:
            ret = f'{ret}.{self.remainder}'
        return ret

    def __add__(self, other):
        """
        Add two ConfigPath's together
        :param other: Another ConfigPath or String that will be parsed as an Config Path
        :return: Combined ConfigPath
        """
        if isinstance(other, (str, ConfigPath)):
            return ConfigPath(f"{str(self)}.{str(other)}")
        elif other is None:
            return self
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")

    def __radd__(self, other):
        """
        Add two ConfigPath's together (with a string on the left)
        :param other: Another ConfigPath or String that will be parsed as an Config Path
        :return: Combined ConfigPath
        """
        if isinstance(other, (str, ConfigPath)):
            return ConfigPath(f"{str(other)}.{str(self)}")
        elif other is None:
            return self
        else:
            raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")


ConfigPathStr = Union[ConfigPath, str]
ConfigPathNoneStr = Union[ConfigPath, str, None]


class RecursiveConfig(BaseModel):
    """
    Recursive Configuration, can store more RecursiveConfigs as children and Options at any level.
    """
    # Configuration ID, usually represents a plugin ID - Ex, 'nc_shell' in 'job.plugins.nc_shell:4444'
    config_id: str
    # name is the unique string identifier when multiple configurations exist. Ex, '4444' in 'job.plugins.nc_shell:4444'
    name: NoneStr = None
    # Optional parent container, should be part of the Config Path - Ex, 'plugins' in 'job.plugins.nc_shell:4444'
    parent: Optional[RecursiveConfig] = None
    # Optional Plugin Class, if this Config is representing a plugin's options
    plugin: Optional[Plugin] = None
    # Sub-configuration objects
    children: List[RecursiveConfig] = Field(default_factory=list)
    options: List[AnyOption] = Field(default_factory=list)
    #: Defaults will be a list of configs, where each config represents a plugin's default values
    #: Each plugin should only be added once. Defaults exist at every level, but for the purpose
    #: of the provisioner, are only implemented at the top Job level
    # todo implement an interface for this later. Basic UI could load all available config_ids and oids,
    #      then allow adding a value to defaults based on a config_id/oid already in use somewhere in the tree
    defaults: List[RecursiveConfig] = Field(default_factory=list)

    # Validator to ensure all options are instantiated correctly
    _options_are_instantiated = validator('options', pre=True, allow_reuse=True)(options_are_instantiated)

    def __init__(self, plugin_store: PluginStore = None, **kwargs):
        """
        Override init to load children correctly. Children are special because they require a reference to their parent,
        so the parent needs to be initialized first. We don't have access to the initialized parent inside validators.
        """
        children = kwargs.pop('children', [])

        super().__init__(**kwargs)
        if plugin_store and self.plugin:
            self._update_plugin(plugin_store)

        for child in children:
            self.add_child_config(plugin_store=plugin_store, **child)

    @property
    def top_config(self):
        """Return the top-most config object. Expected use is mostly by option validation functions."""
        return self.parent.top_config if self.parent else self

    @property
    def path(self) -> str:
        """
        Get Full config path, excluding the top level config
        :return str: Full Config Path
        """
        if not self.parent:
            # topmost config is considered unnamed/unpathed
            return None  # type: ignore
        ret = self.config_id
        if self.parent.path:
            ret = f'{self.parent.path}.{ret}'
        if self.name:
            ret = f'{ret}:{self.name}'
        return ret

    def _update_plugin(self, plugin_store: PluginStore):
        if not self.plugin:
            return  # do this check here for mypy

        updated: Optional[Plugin] = plugin_store.plugins.get(self.plugin.id)
        if not updated:
            print(f"WARNING: could not find updated plugin for ID {self.plugin.id} - The job may fail execution!")
            return

        # We found an updated plugin, reset options and plugin object
        old_options = self.options
        self.plugin = updated
        self.options = [o() for o in updated.options]  # get a copy of all the plugin options

        for opt in old_options:
            # try to find a matching new option
            try:
                new_option = self.get_option(opt.oid)
            except KeyError:
                print(f"WARNING: failed to update option {opt.oid} on {self.plugin.id} - copying into job as-is. "
                      f"This value will likely be ignored by the job.")
                self.options.append(opt)
                continue
            # load value
            new_option.from_tree(opt.tree())
            logging.debug(f'option {new_option.display_name} ({opt.oid}) updated with previous value')
            # self.options.append(opt(**kwargs))

    def _sort_by_altitude(self, plugin):
        try:
            return plugin.option_from_name('altitude').val
        except AttributeError:
            return 10000

    def add_child_config(self, config_id: str, name: NoneStr = None, **kwargs) -> RecursiveConfig:
        """
        Add a Child Configuration at the given ConfigID and Optional Name
        :param config_id: Configuration ID to add
        :param name: Optional Name of Configuration
        :param kwargs: Arguments to RecursiveConfig
        :return: Added Configuration
        """
        if self.child_exists(config_id, name):
            raise KeyError(f'Config already exists! {self.path}.{config_id}:{name}')
        conf = RecursiveConfig(**kwargs, config_id=config_id, name=name, parent=self)
        self.children.append(conf)
        return conf

    def add_plugin(self, plugin: Plugin, name: NoneStr = None) -> RecursiveConfig:
        """
        Add a Child Configuration based on a Plugin Class
        :param plugin: Plugin Class to create Configuration from
        :param name: Optional Name of Plugin Configuration
        :return: Created Child Configuration
        """
        name = name or self._gen_next_name(plugin.id)
        ret = self.add_child_config(options=plugin.options, config_id=plugin.id, name=name, plugin=plugin)
        self.children.sort(key=self._sort_by_altitude)
        return ret

    def option_from_name(self, name: str) -> Optional[AnyOption]:
        """
        Return the Option object given the name of the option
        :param name: Name of the option
        :return: Option object, if the name was found
        """
        ret = None
        for option in self.options:
            if not hasattr(option, 'name'):
                continue

            if option.name == name:
                ret = option

        return ret

    def _gen_next_name(self, cid, generator=range(0, 100000)):
        """
        Generate the next available plugin name
        :param cid: Configuration ID to generate name for
        :param generator: Custom generation function, defaults to a large range of numbers
        :return: Next Available ID for the given Configuration ID
        """
        return next(str(i) for i in generator if not self.child_exists(cid, str(i)))

    def child_exists(self, cid: NoneStr, name: NoneStr) -> bool:
        """
        Check if a child configuration exists. If a parameter is None, it is ignore.
        :param cid: Config ID of the child
        :param name: Name of the Child
        :return bool: True if the child object was found, or neither parameter was specified
        """
        for child in self.children:
            # Start flags as true if they are None
            f_conf = cid is None
            f_name = name is None
            if cid and child.config_id == cid:
                f_conf = True
            if name and child.name == name:
                f_name = True
            if f_conf and f_name:
                return True
        return False

    def get_option(self, path: OptionPathNoneStr) -> AnyOption:
        """
        Get an Option Class matching an Option ID, optionally traversing up the tree to find a default value
        :param path: Path of Option ID to return
        :param defaults: If true and the OptionID at the current level does not have a value set,
                         the tree will be traversed upward to check for a default value for the option.
        :return: Option class matching criteria
        :raises ValueError: When an Option resolves to a Simple Option but a key or remainder is provided
        """
        option = None
        path = OptionPath(path)
        # first, check for the Option ID within the current config
        for opt in self.options:
            if opt.oid == path.oid:
                option = opt

        # if option is None and defaults:
        #     # todo also check for a default value if the value is None/Falsey?
        #     # todo this code is entirely untested
        #     # Second, check for a default option within a parent
        #     opt = self.get_default(self.config_id, path.oid)
        #     if opt:
        #         option = opt

        if option is not None:
            # A simple option will return itself, or raise a ValueError if a path/remainder is supplied
            return option.get_option(path)

        raise KeyError(f"oid not found: {path.oid}")

    # def get_default(self, path: ConfigPathStr, oid) -> Optional[AnyOption]:
    #     """Get a default value from a parent configuration"""
    #     # fixme consider this unimplemented/untested
    #     path = ConfigPath(path)
    #     for conf in self.defaults:
    #         if conf.config_id != path.config_id:
    #             continue
    #         for opt in conf.options:
    #             if opt.oid == oid and opt.value:
    #                 return opt
    #     if self.parent:
    #         return self.parent.get_default(path.config_id, oid)
    #     return None

    # todo search options? this only completes configs, not options
    def search_configs(self, prefix: ConfigPathStr) -> List[str]:
        """
        Search config IDs and Names - used for UI completion functions. Will ignore the top-level config_id (ex, `job`)
        :param prefix: Config path prefix to search for
        :return: List of config paths that match the search path
        """
        path = ConfigPath(prefix)
        ret = []
        for child in self.children:
            # if remainder exists, the config id must match exactly
            if path.remainder and child.config_id != path.config_id:
                continue
            # if there isn't a remainder, and config id starting with the right characters is good 'nuff
            # or '' allows us to complete all options given config_id=None
            if not child.config_id.startswith(path.config_id or ''):
                continue
            # same store with name - must match exactly if it exists and there is a remainder.
            # the name is not required at all, however
            if path.name and child.name and path.remainder and child.name != path.name:
                continue
            if path.name and child.name and not child.name.startswith(path.name):
                continue
            # found one!

            if path.remainder:
                # check if the remainder of the search matches
                ret.extend(child.search_configs(path.remainder))
            else:
                # no more prefix left, we're the end of the search.
                # Add ourselves to the results, and add all children configs
                ret.append(child.path)
                # This is a bit of a hack, but this will add all children of the child config to the list of results.
                # By sending the grandchild config_id, we will always end up on this same code path until
                # the bottom of the tree.
                for grandchild in child.children:
                    ret.extend(child.search_configs(grandchild.config_id))
        return ret

    def del_conf(self, path: ConfigPathStr, what_if=False, _all=False) -> int:
        """
        Delete a Child Configuration at the given path.
        Will recursively delete if child Configurations exist at the path.
        :param path: Path to the Configuration to Delete
        :param what_if: Don't actually delete, but count how many deletions would occur
        :param _all: Internal Parameter to delete all children configurations
        :return: Number of Configurations Deleted [or would be deleted]
        """
        path = ConfigPath(path)

        count = 0
        for idx, child in enumerate(self.children):
            if _all:
                # If all, delete the list, because iterating over it and deleting elements is bad, M'kay
                count = len(self.children)
                if child.children:
                    count += child.del_conf(ConfigPath(child.config_id), what_if=what_if, _all=True)
                if not what_if:
                    del self.children
                    self.children = []
                break

            # Check if the path matches the current child
            if child.config_id != path.config_id:
                continue
            if path.name and child.name != path.name:
                continue

            if path.remainder:
                # Remainder indicates we're not deep enough to delete anything yet
                count += child.del_conf(path.remainder, what_if=what_if)
            else:
                # Delete self and all child elements
                count += 1
                if child.children:
                    count += child.del_conf(ConfigPath(child.config_id), what_if=what_if, _all=True)
                if not what_if:
                    del self.children[idx]

        if count == 0:
            raise KeyError(f'Config path not found: {self.path + "." if self.path else ""}{path}')

        return count

    def resolve_config(self, path: ConfigPathNoneStr) -> RecursiveConfig:
        """
        Resolve a configuration by path. Will ignore the top-level config_id (ex, `job`)

        If Config name is not provided and multiple matches are found, a ValueError is raised.

        :raises KeyError: If the path is not found
        :raises KeyError: If multiple matches are found.  This will happen when Config name is omitted from the path.
        """
        path = ConfigPath(path)

        result = None
        for child in self.children:
            if child.config_id != path.config_id:
                continue
            if path.name and child.name != path.name:  # todo require name for resolve?
                continue
            # Found one!
            if path.remainder:
                tmp = child.resolve_config(path.remainder)
            else:
                tmp = child

            if result:
                raise KeyError('Multiple matching configurations resolved!')
            result = tmp

        if not result:
            raise KeyError(f'Config path not found: {self.path + "." if self.path else ""}{path}')

        return result

    def resolve_option(self, config_path: ConfigPathNoneStr, option_path: OptionPathNoneStr) -> AnyOption:
        """
        Resolve an Option class based on a path and an option ID.
        Will ignore the top-level config_id in the path (ex, `job`)

        :param config_path: Config Path to retrieve the Option from
        :param option_path: Option ID to search for (or OptionPath, given a MultiOption)
        :return: Option Class matching Config Path and Option ID
        :raises KeyError: When a config path or option ID couldn't be found
        """
        return self.resolve_config(config_path).get_option(option_path)

    def check(self, job: Job) -> Dict[str, Union[Dict[str, List[str]], List[str]]]:
        """
        Validate all options and children. This method should be called top-down
        :return: Tree of config_ids/option_ids as keys and Lists of Errors as values
        """
        ret = {}
        for opt in self.options:
            # todo how do we handle errors nested within a MultiOption?
            #      Pass OptionPath into a a Custom Exception maybe?
            errors = opt.check(job, self)
            if errors:
                # todo this creates one flat dictionary, which may not be what we want - we might want these as a tree
                ret[f"{self.path}!{opt.oid}"] = errors
        for conf in self.children:
            errors = conf.check(job)  # type: ignore
            if errors:
                ret.update(errors)  # type: ignore
        return ret  # type: ignore

    def tree(self, by_var=False) -> Dict:
        """
        Generate a tree of Options. Normal Options will be represented as {oid: value}, while
        MultiOptions will be represented as {pkey.value: {item.oid: item.val for item in items}
        :param by_var: Use `var` attribute in place of OID. Does not affect pkey of MultiOptions.
        :return dict: Tree representing config Options
        """
        ret = {}
        for opt in self.options:
            if opt.tree() is None:
                continue  # Skip null values
            ret[getattr(opt, 'var', opt.oid) if by_var else opt.oid] = opt.tree()
        return ret

    @exclude_fields('parent')
    def dict(self, *args, **kwargs):
        """
        Override the dict function to exclude the parent attribute
        """
        return super().dict(*args, **kwargs)


class Plugin(BaseModel):
    """
    Generic Plugin Type
    """
    name: str = Field(alias='Title')
    id: str
    #: Dictionary representing how the Plugin should be used in Ansible
    # {'include_role': {'name': role_name}} or {c2games.opfor.<module>: {Opts}}
    usage: Dict[str, Dict[str, Any]]
    provides: List[str] = Field(default_factory=list)
    depends: List[str] = Field(default_factory=list)
    #: Source File Plugin was loaded from
    source: Optional[Path] = None
    source_dir: Optional[Path] = None
    options: List[AnyOption] = Field(default_factory=list, allow_mutation=False, alias='Options')

    # Validator to ensure all options are instantiated correctly
    _options_are_instantiated = validator('options', pre=True, allow_reuse=True)(options_are_instantiated)

    def get_usage(self, name, task_vars):  # pylint: disable=unused-argument
        """Return usage string with any modifications needed in subclasses"""
        return deepcopy(self.usage)


class C2GamesMeta(Plugin):
    """
    C2 Games YAML File Metadata
    """
    # todo enum[Misconfiguration,server,etc]
    # todo List[class OSCompatibility]
    # todo List[class ExploitationExample]
    source_dir: Path  # redefine source_dir as required
    spec_version: Optional[str] = Field(default=None, alias='SpecVersion')
    version: Optional[str] = Field(default=None, alias='Version')
    author: Optional[str] = Field(default=None, alias='Author')
    organization: Optional[str] = Field(default=None, alias='Organization')
    type: Optional[str] = Field(default=None, alias='Type')
    difficulty: Optional[int] = Field(default=None, alias='Difficulty')
    description: Optional[str] = Field(default=None, alias='Description')
    os_compatibility: Optional[List[Dict]] = Field(default_factory=list, alias='OSCompatibility')
    exploitation: Optional[List[Dict]] = Field(default_factory=list, alias='Exploitation')
    mitigation: Optional[str] = Field(default=None, alias='Mitigation')
    training: Optional[str] = Field(default=None, alias='Training')
    justification: Optional[str] = Field(default=None, alias='Justification')
    spec: Dict[str, Any] = Field(default_factory=dict, alias='Spec')
    nice_categories: List[str] = Field(default_factory=list, alias='NiceCategories')
    mitre_categories: List[str] = Field(default_factory=list, alias='MitreCategories')
    ansible_collections: List[str] = Field(default_factory=list, alias='AnsibleCollections')

    _coerce_oid_from_options = validator('options', pre=True, allow_reuse=True)(coerce_oid_from_options)


# pydantic things ¯\_(ツ)_/¯
RecursiveConfig.update_forward_refs()
ConfigPath.update_forward_refs()
