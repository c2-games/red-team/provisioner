import importlib
import importlib.util

from importlib.abc import Loader
from importlib.machinery import ModuleSpec
from pathlib import Path
from types import ModuleType
from typing import Optional, Union

from provisioner.structs.plugins import get_plugin_name, Plugin
from provisioner.loaders import PluginLoader


class PythonPluginLoader(PluginLoader):
    """
    Load python files dynamically that contain an __plugin__ attribute
    and add them to the plugin store
    """
    PLUGIN_SUFFIXES = ['.py']

    def load_plugin(self, path: Union[Path, str], name: Optional[str] = None) -> Optional[Plugin]:
        if not name:
            name = get_plugin_name(str(path), self.plugin_basedir)
        mod_name = name.replace('/', '.')
        try:
            mod = self._load_plugin(path, mod_name)
            self.logger.debug(f"successfully loaded module {mod.__name__}")
            name, mod, plugin = self._plugin_store.add_plugin_from_module(mod, name)
            if plugin:
                self.logger.debug(f"successfully added plugin {plugin}")
            return plugin
        except Exception as e:
            self.logger.error(f"Could not load plugin {path}: {e}")
            return None

    @staticmethod
    def _load_plugin(path: Union[Path, str], name: str) -> ModuleType:
        """
        Load a module using importlib

        :param path: Path to file, including file extension
        :param name: Name to give spec - can be arbitrary, but should be full path to module.
                     If None, a name will be generated using the path relative to the project root
                     Ex: provisioner.plugins.persistence.at
        :return: Imported module
        """
        spec: ModuleSpec = importlib.util.spec_from_file_location(name, str(path))  # type: ignore
        mod: ModuleType = importlib.util.module_from_spec(spec)
        loader: Optional[Loader] = spec.loader
        loader.exec_module(mod)  # type: ignore
        return mod
