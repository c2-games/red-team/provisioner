from pathlib import Path
from typing import Optional, Type, Union

import json
import yaml

from provisioner.structs.plugins import get_plugin_name, Plugin, C2GamesMeta
from provisioner.loaders import PluginLoader


class YamlPluginLoader(PluginLoader):
    """
    Load C2Games YAML files from disk.

    This class assumes the directory of a c2games.yml file is an Ansible Role.
    Files must _END_ with c2games.yml, but can be prefixed with any string.
    """
    PLUGIN_SUFFIXES = ['c2games.yml', 'c2games.yaml']

    def __init__(self, *args, target_class: Type[Plugin] = C2GamesMeta, **kwargs):
        self.target_class = target_class
        PluginLoader.__init__(self, *args, **kwargs)

    def load_plugin(self, path: Union[Path, str], name: Optional[str] = None) -> Optional[Plugin]:
        """
        Load YAML file into `target_class` and add it to the PluginStore.
        :param path: Parent directory to search for plugins
        :param name: Name to import plugin as. A name will be generated based on file
                    path and plugin_basedir if not provided using `get_plugin_name`
        """
        path = Path(path)  # Ensure we have a path object
        if not name:
            name = get_plugin_name(str(path.parent), self.plugin_basedir)
        # hardcoded string is meh, but it works well
        plug_id = name
        id_prefix = path.name.split('c2games')[0]
        if id_prefix and id_prefix.endswith('.'):
            id_prefix = id_prefix[:-1]
        if id_prefix:
            # generate an ID as 'path/to/{id_prefix}/ansible_role', where id_prefix is the prefix to c2games.yml
            plug_id = '/'.join([*plug_id.split('/')[:-1], id_prefix, plug_id.split('/')[-1]])

        try:
            with open(path) as f:
                contents: dict = yaml.safe_load(f)
            if not contents:
                raise ValueError('File was empty')
            if not isinstance(contents, dict):
                raise ValueError('Contents was not loaded as a dict, assuming malformed!')
        except Exception as e:
            self.logger.error(f"could not read YAML file '{path}': {e}")
            return None

        # add a altitude option to all c2games YAML plugins
        if not contents.get('Options'):
            contents['Options'] = {}

        # Give a default of 10000 but allow c2games.yml to set its own altitude
        if contents['Options'].get('altitude'):  # only write out meta data
            contents['Options']["altitude"].update({
                "name": "altitude", "type": "int",
                "description": "Decides when a plugin will run. Lower value runs first."
            })
        else:
            contents['Options']["altitude"] = {
                "name": "altitude", "type": "int", "default": 10000,
                "description": "Decides when a plugin will run. Lower value runs first."
            }

        try:
            # Create a new C2Games plugin from a c2games.yaml file
            # We must dump to JSON and use parse_raw here so we can take advantage of our custom JSON Decoders
            meta = C2GamesMeta.parse_raw(json.dumps({
                # Splat all the YAML contents into a new dict
                **contents,
                # Convert Path() objects to string, json.dumps doesn't know how to serialize them
                'id': plug_id, 'source': str(path), 'source_dir': str(path.parent),
                'usage': {'include_role': {'name': name}}
            }))
            # Set source file of plugin
        except Exception as e:
            self.logger.error(f"could not generate Plugin class for '{path}': {e}")
            return None

        try:
            self.plugin_store.add_plugin(plug_id, meta, provides=meta.provides or [])
        except Exception as e:
            self.logger.error(f"could not add Plugin to Plugin Store '{path}': {e}")
            return None

        return meta
