import json
from typing import Dict

from bson.codec_options import TypeCodec

RANGE_KEY = '__range__'


class RangeCodec(TypeCodec):
    """Custom Mongo Codec to encode and decode range objects"""
    python_type = range
    bson_type = dict

    def transform_python(self, value):
        return encode_range_object(value)

    def transform_bson(self, value):
        return object_hook(value)


def object_hook(dct: Dict):
    """Decode complex values from JSON, such as Python Range objects."""
    # Only dictionary key is RANGE_KEY, it's an encoded python range
    # We're expecting {RANGE_KEY: {'start': int, 'stop': int, 'step': int}}
    if RANGE_KEY in dct and len(dct) == 1:
        args = []
        data = dct[RANGE_KEY]

        # Array and Nesting is required because arguments to range cannot be kwargs,
        # so we must splat them in the correct order
        if data.get('start') is None:
            # If we don't have a start available, just return the original value, FAIL!
            return dct

        args.append(data['start'])
        if data.get('stop') is not None:
            args.append(data['stop'])
            if data.get('step') is not None:
                args.append(data['step'])
        return range(*args)

    return dct


def encode_range_object(rng: range):
    """Custom function to encode Python range objects."""
    return {RANGE_KEY: {'start': rng.start, 'stop': rng.stop, 'step': rng.step}}


class JSONDecoder(json.JSONDecoder):
    """Custom JSONDecoder to override object hook kwarg."""
    def __init__(self, *args, **kwargs):
        """Override init with custom object hook."""
        json.JSONDecoder.__init__(self, object_hook=object_hook, *args, **kwargs)
