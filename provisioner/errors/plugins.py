class PluginAlreadyExistsError(Exception):
    """
    Plugin Exists within Plugin Store
    """


class PluginNotLoadedError(Exception):
    """
    The accessed plugin could not be found
    """
