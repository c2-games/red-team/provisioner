# Configurable Options in a Plugin

----------------------------------

Configuring a plugin to use configurable options is not terribly hard. To
create configurable options, you will need to:

* edit your c2games.yml file in a specific way the provisioner project
  understands
* align the defaults/main.yml variables and variables used in your tasks
  appropriately

## SSH Authorized Keys Example

SSH Authorized Keys has two options, a configurable authorized key, and the
locations it will be installed to.

### Creating a new role

Follow along with [creating-a-challenge][1] for how to create a new ansible
role, if needed, then continue reading.

### Configuring the c2games.yml file

Configurable options have a few keys that need to be defined in order for the
provisioner to identify and use them. An example are the `ssh_authorized_keys`
options shown below. The primary keys `key_locations` and `malicious_public_key`
are the names of the options the provisioner will see. The keys defined beneath
them will be interpretted by the provisioner and are required for proper usage.

```yml
  Options:
    key_locations:
      type: list
      default: ['/root/.ssh/authorized_keys']
      required: true
      name: Key Locations
      description: Path to install authorized keys to. WARNING ansible will
                   change the parent directory permissions to match what
                   .ssh needs(0700)!!!
      var: key_locations
    malicious_public_key:
      type: str 
      # yamllint disable-line rule:line-length
      default: "<PUBLIC SSHKEY>"
      required: true
      name: Malicious Public Key 
      description: Public key to place in authorized hosts
      var: malicious_public_key
```

The required keys are:

* type - The variable type of an option. Examples would be: `list`, `str`,
  `int`, etc.
* default - The default value provisioner should use. _CURRENTLY values in
  defaults/main.yml IS NOT USED!_
* required - Whether or not the option needs to be defined
* name - Human friendly name of the option
* description - Human friendly description of the option
* var - variable name ansible will recognize as a variable to use. Should align
  with defaults/main.yml and tasks variables used

### Use the correct variable

The value specified in the `var` keyword is the variable that ansible is going
to be supplied through the provisioner. So utilize this variable in any tasks
or files for the role being used. As seen above, you can have the `var` value
be the same as the option that the provisioner will see.

## Option types

There are various option types available to use.

### int

Integer value. Will convert strings/floats to int if possible.

### port

Inherits from int, but only accepts value port ranges from 0 to 65535.

### bool

Boolean value. Will convert strings of "yes/no", "y/n", "true/false" if
possible.

### list

List option.

### set

Set option. Like a list, but a logical set and only containing unique items.

### multi

Multi option which can contain multiple configurable options within it.
Examples would include the NFS role which can configure multiple hosts and
directory paths to export via NFS.

## Testing your role and options

Once the role is finished, you can run the cli.py file and test using your role
with the new option.

```bash
python provisioner/cli.py

# Plugins are tab-completable!
use <role name>

# Options are also tab-completable!
set plugins.<role name>:0!<option name> <value>

# Display option value
set --show plugins.<role name>:0!<option name>
```

Once satisfied, you can deploy the role to your defined targets and ensure your
options are correctly interpretted in ansible.

[1]: https://www.ncaecybergames.org/creating-a-challenge/
